# CreoEx

#### 介绍

Creo 二次开发

### 开发环境

1. 工具：VScode
2. Creo 版本：Creo3.0M130
3. VS 版本：VS2012
4. 编译器:nmake
5. 语言：C++

### 使用说明

1. CreoEx 中包含插件运行所需文件，其中 install.bat 可自动生成 creotk.dat
2. 插件基于 Creo3.0 开发，理论上此版本以上的 Creo 均可正常运行
3. 插件运行过程会在 D:\PTC 下生成一个 exception_info.inf 文件，主要用于输出异常信息和开发过程中的一些调试信息。

### 开发说明

1. 由于 Creo 二次开发参考资料有限，此插件为本人通过有限的资源结合官方说明文档慢慢摸索出来的，可能存在 BUG，欢迎指出，一起交流学习
2. 本人对于 C++的 Interface 和 Listener 理解不深，曾尝试通过实现 Interface 创建自己的类，内置类在实现接口过程中有 xaideclare 和 xrchandle 两个宏定义，看不懂，最终以失败告终。对于 Listener 的尝试部分成功。欢迎大牛指导。
3. dll 的调试目前还没有找到合适的办法，目前只是采用最笨的办法，希望对 dll 调试有经验的大牛不吝赐教。
4. 该插件编写的初衷是为了实现工程图的自动球标功能，也是出于个人的兴趣爱好，后来又慢慢摸索扩展了很多功能。功能不一定实用，但是探索的过程很有意思，没完成一个功能也是成就感满满。
