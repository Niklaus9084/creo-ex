#include <OTKXMain.h>
//遍历模型树
void _Traversal_ModelTree(pfcModel_ptr Model, pfcModels_ptr &Models)
{
    OPENEXCEPTIONFILE("a");
    pfcSession_ptr Session = pfcGetProESession();
    pfcSolid_ptr Solid = pfcSolid::cast(Model);
    pfcFeatures_ptr Features = Solid->ListFeaturesByType(true, pfcFEATTYPE_COMPONENT);
    try
    {
        for (xint i = 0; i < Features->getarraysize(); i++)
        {
            pfcFeature_ptr Feature = Features->get(i);
            pfcComponentFeat_ptr Component = pfcComponentFeat::cast(Feature);
            pfcModelDescriptor_ptr ModelDescr = Component->GetModelDescr();
            pfcModelType ModelType = ModelDescr->GetType();
            pfcModel_ptr ComponentModel = Session->RetrieveModel(ModelDescr);
            Models->append(ComponentModel);
            if (ModelType == pfcMDL_ASSEMBLY)
            {
                _Traversal_ModelTree(ComponentModel, Models);
            }
        }
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (exception e)
    {
        fprintf(exception_info, "Msg:%s\n", e.what());
    }
    fclose(exception_info);
}
//遍历特征树
void _Traversal_ComponentTree(pfcModel_ptr Model, pfcFeatures_ptr &Features)
{
    OPENEXCEPTIONFILE("a");
    pfcSession_ptr Session = pfcGetProESession();
    pfcSolid_ptr Solid = pfcSolid::cast(Model);
    pfcFeatures_ptr CFeatures = Solid->ListFeaturesByType(true, pfcFEATTYPE_COMPONENT);
    try
    {
        for (int f = 0; f < CFeatures->getarraysize(); f++)
        {
            pfcFeature_ptr Feature = CFeatures->get(f);
            pfcComponentFeat_ptr Component = pfcComponentFeat::cast(Feature);
            pfcModelDescriptor_ptr ModelDescr = Component->GetModelDescr();
            pfcModelType ModelType = ModelDescr->GetType();
            pfcModel_ptr ComponentModel = Session->RetrieveModel(ModelDescr);
            if (ModelType == pfcMDL_ASSEMBLY)
            {
                _Traversal_ComponentTree(ComponentModel, Features);
            }
            else
            {
                Features->append(CFeatures->get(f));
            }
        }
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (exception e)
    {
        fprintf(exception_info, "Msg:%s\n", e.what());
    }
    fclose(exception_info);
}