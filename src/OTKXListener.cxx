#include <OTKXMain.h>
#include <uifcDefaultListeners.h>
//Session:pfcSessionActionListener
void SessionAction::OnAfterWindowChange(optional pfcWindow_ptr NewWindow)
{
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "Session Action Test Succ!\n");
    fclose(exception_info);
}
void SessionAction::OnAfterModelDisplay()
{
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "SessionDodelDis Action Test Succ!\n");
    fclose(exception_info);
}
//ModelAction:pfcModelActionListener
void ModelAction::OnAfterModelRename(pfcModel_ptr FromMdl, pfcModel_ptr ToMdl)
{
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "Model Action Test Succ!\n");
    // fprintf(exception_info, "FromMdlName:%s\tToMdlName:%s\n", FromMdl->GetFullName(), ToMdl->GetFullName());
    fclose(exception_info);
}
void ModelAction::OnAfterModelSave(pfcModelDescriptor_ptr Descr)
{
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "ModelSave Action Test Succ!\n");
    fclose(exception_info);
}
void ModelAction::OnBeforeModelDisplay(pfcModel_ptr Mdl)
{
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "ModelDis Action Test Succ!\n");
    fclose(exception_info);
}
void ModelAction::OnAfterModelRetrieve(pfcModel_ptr Mdl)
{
    xstring directory = Mdl->GetDescr()->GetPath();
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "ModelRet Action Test Succ!\n");
    // fprintf(exception_info, "FilePath:%s\n", directory);
    fclose(exception_info);
}
//ModelEvent:pfcModelEventActionListener
void ModelEvent::OnAfterModelCopy(pfcModelDescriptor_ptr FromMdl, pfcModelDescriptor_ptr ToMdl) {}

void ModelEvent::OnAfterModelCopyAll(pfcModelDescriptor_ptr FromMdl, pfcModelDescriptor_ptr ToMdl) {}

void ModelEvent::OnAfterModelRename(pfcModelDescriptor_ptr FromMdl, pfcModelDescriptor_ptr ToMdl)
{
    pfcSession_ptr session = pfcGetProESession();
    OPENEXCEPTIONFILE("a");
    fprintf(exception_info, "Listener Test Succ!\n");
    fclose(exception_info);
}

void ModelEvent::OnAfterModelErase(pfcModelDescriptor_ptr Mdl) {}

void ModelEvent::OnAfterModelDelete(pfcModelDescriptor_ptr Mdl) {}