#include <OTKXMain.h>

//修改尺寸位置 纠正SetLocation函数设置尺寸位置时产生偏移
void ChangeDimensionLocation(pfcDimension2D_ptr dimension, pfcPoint3D_ptr dimloc)
{
    dimension->SetLocation(dimloc);
    //纠正更改尺寸时产生的偏差
    pfcPoint3D_ptr dimlocnew = dimension->GetLocation();
    double dx = dimloc->get(0) - dimlocnew->get(0); //计算x偏移值
    double dy = dimloc->get(1) - dimlocnew->get(1); //计算y偏移值
    dimloc->set(0, dimloc->get(0) + dx);            //修正x偏移
    dimloc->set(1, dimloc->get(1) + dy);            //修正y偏移
    dimension->SetLocation(dimloc);
}
//计算尺寸字符占位宽度
int CalDimensionValueWideth(pfcDimension2D_ptr dimension)
{
    OPENEXCEPTIONFILE("a");
    int tpad = 0;
    smatch result;
    regex pattern("\\d+:");
    xstringsequence_ptr texts = dimension->GetTexts();

    //判断是否有公差
    xbool TolIsDisplay = dimension->GetIsToleranceDisplayed();
    if (TolIsDisplay)
    {
        tpad = 2 * pad;
    }
    //判断是否存在符号
    for (xint j = 0; j < texts->getarraysize(); j++)
    {
        string text = texts->get(j); //{0:⌀}{1:@D}
        string::const_iterator iterStart = text.begin();
        string::const_iterator iterEnd = text.end();
        int temp = 0;
        //正则匹配\d+:的数量
        while (regex_search(iterStart, iterEnd, result, pattern))
        {
            temp += 1;
            iterStart = result[0].second;
        }
        if (temp > 1)
            tpad += (temp - 1) * pad;
    }
    xreal DimValue = dimension->GetDimValue();
    if (DimValue < 1.0)
        DimValue += 1.0;
    int int_dimvalue = round(DimValue * 100); //保留两位小数
    //判断数值占位宽度
    for (int t = 0; t < 2; t++) //处理小数部分
    {
        if (int_dimvalue % 10 != 0)
        {
            tpad += 0.5 * pad; //小数点占位
            break;
        }
        int_dimvalue /= 10;
    }
    while (int_dimvalue != 0) //处理整数部分
    {
        int_dimvalue /= 10;
        tpad += 0.5 * pad;
    }
    fclose(exception_info);
    return tpad;
}
//线性尺寸居中 仅能计算水平和竖直尺寸
void SetDimensionMid(pfcDimension2D_ptr dimension, pfcPoint3Ds_ptr &Points)
{
    OPENEXCEPTIONFILE("a");
    pfcSession_ptr session = pfcGetProESession();
    pfcModel_ptr model = session->GetCurrentModel();
    pfcDrawing_ptr drawing = pfcDrawing::cast(model);

    if ((dimension->GetDimType() == pfcDIM_LINEAR))
    {
        xint PointsNum = -1;
        pfcSelections_ptr AttachmentPoints;

        AttachmentPoints = dimension->GetAttachmentPoints();
        PointsNum = AttachmentPoints->getarraysize();
        pfcView2D_ptr view = dimension->GetView();
        xreal viewscal = view->GetScale();
        pfcTransform3D_ptr ViewTrans = view->GetTransform();
        pfcDetailItem_ptr DetailItem;
        xint sheetNumber = drawing->GetCurrentSheetNumber();
        pfcTransform3D_ptr SheetTrans;
        SheetTrans = drawing->GetSheetTransform(sheetNumber); //用之前计算，防止后面求逆后出错；后面循环中不存在求逆操作
        for (int a = 0; a < 2; a++)
        {
            pfcPoint3D_ptr tmp_point = AttachmentPoints->get(a)->GetPoint(); //连接点坐标
            pfcModel_ptr AttachModel = AttachmentPoints->get(a)->GetSelModel();
            pfcComponentPath_ptr ComponentPath = AttachmentPoints->get(a)->GetPath();

            //装配图 连接点坐标需从零件转到装配坐标，此处可能存在bug
            if (ComponentPath)
            {
                pfcTransform3D_ptr ComponentTrans = ComponentPath->GetTransform(false);
                tmp_point = ComponentTrans->TransformPoint(tmp_point);
            }

            if (AttachModel->GetType() != pfcMDL_DRAWING)
            {
                tmp_point = ViewTrans->TransformPoint(tmp_point); //连接点为模型中元素时，需要根据视图变换矩阵转换坐标
            }
            tmp_point = SheetTrans->TransformPoint(tmp_point); //屏幕坐标和图框内坐标的坐标转换

            Points->append(tmp_point);
        }
        xreal dimvalue, dx, dy;
        dimvalue = dimension->GetDimValue();
        dx = abs(Points->get(0)->get(0) - Points->get(1)->get(0));
        dy = abs(Points->get(0)->get(1) - Points->get(1)->get(1));
        _direction flag = Direction_nil;
        if (abs(dimvalue * viewscal - dy) < eps)
        {
            flag = Vertical;
        }
        else if (abs(dimvalue * viewscal - dx) < eps)
        {
            flag = Horizon;
        }
        //调整水平或竖直尺寸居中
        if (flag != -1)
        {
            //此处屏幕坐标和图纸坐标的变换没有搞清楚
            SheetTrans = drawing->GetSheetTransform(sheetNumber); //作用域控制在当前语句块范围内
            SheetTrans->Invert();
            pfcPoint3D_ptr p1, p2, loc;
            p1 = SheetTrans->TransformPoint(Points->get(0));
            p2 = SheetTrans->TransformPoint(Points->get(1));
            loc = dimension->GetLocation();
            //尺寸在尺寸界限之间 将尺寸居中放置
            if ((p1->get(flag) - loc->get(flag)) * (p2->get(flag) - loc->get(flag)) < 0)
            {
                loc->set(flag, (p1->get(flag) + p2->get(flag)) / 2);
                ChangeDimensionLocation(dimension, loc);
                loc = dimension->GetLocation();
            }
        }
    }
}
//DimensionStruct成员函数
pfcDimension2D_ptr DimensionStruct::GetDimension()
{
    return dimension;
}
void DimensionStruct::Create(pfcDimension2D_ptr dim)
{
    this->dimension = dim;
    this->location = dim->GetLocation();
    this->view2d = dim->GetView();
    this->viewname = view2d->GetName();
    this->viewcenter = GetViewCenter();
    this->angle = GetAngle();
    this->distance = GetDistance();
}
pfcPoint3D_ptr DimensionStruct::GetViewCenter()
{
    pfcOutline3D_ptr outline = this->dimension->GetView()->GetOutline();
    viewcenter = pfcPoint3D::create();
    //计算视图中心点
    for (xint k = 0; k < 3; k++)
    {
        pfcPoint3D_ptr p1, p2;
        p1 = outline->get(0);
        p2 = outline->get(1);

        xreal midvalue = (p1->get(k) + p2->get(k)) / 2;
        viewcenter->set(k, midvalue);
    }
    return viewcenter;
}
double DimensionStruct::GetAngle()
{
    pfcPoint3D_ptr loc = this->dimension->GetLocation();
    double anglearc;
    anglearc = atan2((loc->get(1) - this->viewcenter->get(1)), (loc->get(0) - this->viewcenter->get(0))); //弧度值
    double angledeg;                                                                                      //角度值
    if (anglearc < 0)
        angledeg = anglearc * 180 / PI + 360;
    else
        angledeg = anglearc * 180 / PI;
    anglearc = anglearc - 90;
    if (anglearc < 0)
        anglearc = anglearc + 360;
    angle = round(angledeg / 10) * 10;
    return angle;
}
double DimensionStruct::GetDistance()
{
    pfcPoint3D_ptr loc = this->dimension->GetLocation();
    double distance;
    distance = pow((pow((loc->get(0) - this->viewcenter->get(0)), 2) + pow((loc->get(1) - this->viewcenter->get(1)), 2)), 0.5);
    return distance;
}