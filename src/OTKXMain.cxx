#include <OTKXMain.h>
#include "TechReq.cxx"
#include "AutoIndex.cxx"
#include "OTKXOneKeyAlign.cxx"
#include "SketchAux.cxx"
#include "DrawingAux.cxx"
#include "TableAux.cxx"
#include "AssemblyAux.cxx"

HMODULE GetSelfModuleHandle()
{
	MEMORY_BASIC_INFORMATION mbi;
	return ((::VirtualQuery(GetSelfModuleHandle, &mbi, sizeof(mbi)) != 0) ? (HMODULE)mbi.AllocationBase : NULL);
}
extern "C" _declspec(dllexport) int GetDllPath(string &Path)
{
	char curdir[MAX_PATH] = {};
	//string DellPath;
	GetModuleFileNameA(GetSelfModuleHandle(), curdir, 100);
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	_splitpath(curdir, drive, dir, fname, ext);
	Path = (string)drive + (string)dir;
	return 0;
}
extern "C"
{
	typedef wfcStatus (*myCallback)();
}

class MycallbackClass : public virtual pfcUICommandActionListener
{
public:
	MycallbackClass(myCallback func) { m_func = func; }

	void OnCommand()
	{
		m_func();
	}

private:
	myCallback m_func;
};
int CheckEnv()
{
	pfcSession_ptr session = pfcGetProESession();
	pfcModels_ptr Models = session->ListModels();
	pfcModel_ptr Model = session->GetCurrentModel();
	if (Models->getarraysize() == 0 || Model == NULL)
	{
		session->UIDisplayLocalizedMessage(MSGFILE, "Model is not found in current session!", NULL);
		return 0;
	}
	else if (Model->GetType() != pfcMDL_DRAWING)
	{
		session->UIDisplayLocalizedMessage(MSGFILE, "ModelType is error!", NULL);
		return 0;
	}
	else
		return 1;
}
int CheckEnv(pfcModelType ModelType)
{
	// ofstream exception_info;
	// exception_info.open("exception_info.inf", ios::out);
	pfcSession_ptr session = pfcGetProESession();
	pfcModels_ptr Models = session->ListModels();
	pfcModel_ptr Model = session->GetCurrentModel();
	if (Models->getarraysize() == 0 || Model == NULL)
	{
		session->UIDisplayLocalizedMessage(MSGFILE, "Model is not found in current session!", NULL);
		return 0;
	}
	else if (Model->GetType() != ModelType)
	{
		session->UIDisplayLocalizedMessage(MSGFILE, "ModelType is error!", NULL);
		return 0;
	}
	else
		return 1;
}

int CheckEnv(pfcModelType ModelType1, pfcModelType ModelType2)
{
	// ofstream exception_info;
	// exception_info.open("exception_info.inf", ios::out);

	pfcSession_ptr session = pfcGetProESession();
	pfcModels_ptr Models = session->ListModels();
	pfcModel_ptr Model = session->GetCurrentModel();
	pfcModelType ModelType;
	if (Models->getarraysize() == 0 || Model == NULL)
	{
		session->UIDisplayLocalizedMessage(MSGFILE, "Model is not found in current session!", NULL);
		return 0;
	}
	else if ((Model->GetType() == ModelType1) || (Model->GetType() == ModelType2))
	{

		return 1;
	}
	session->UIDisplayLocalizedMessage(MSGFILE, "ModelType is error!", NULL);
	return 0;
}
wfcStatus ShowDllPath()
{
	OPENEXCEPTIONFILE("a");
	fprintf(exception_info, "DLLPath:%s\n", DllPath.c_str());
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}

wfcStatus Test()
{
	pfcSession_ptr session = pfcGetProESession();
	pfcModel_ptr mdl = session->GetCurrentModel();
	mdl->AddActionListener(new ModelAction());

	session->AddActionListener(new SessionAction());

	return wfcTK_NO_ERROR;
}

wfcStatus CheckModelType();

wfcStatus AutoIndex();
wfcStatus CleanIndex();
wfcStatus CheckIndex();
wfcStatus Test();
wfcStatus SetPad();
wfcStatus VerticalAlign();
wfcStatus HorizonAlign();
wfcStatus VDistribution();
wfcStatus HDistribution();
wfcStatus SetDistance();
wfcStatus SetSymDir();
wfcStatus AddPlasticTechReq();
wfcStatus AddSheetmetalTechReq();
wfcStatus AddCastingTechReq();
wfcStatus AddCenterLine();
wfcStatus SetLength();
wfcStatus SetAttribute();
wfcStatus JoinLine();
wfcStatus AddSymmetryLine();
wfcStatus AddCenterLine2P();
wfcStatus ExtendLine();
wfcStatus ViewTableInfo();
wfcStatus ChangeTable();
wfcStatus CreateTable();
wfcStatus GetCornerCoor();
wfcStatus OpenDrawing();
wfcStatus MoveDrawing();
wfcStatus Drw2Pdf();
wfcStatus Export2Stp();
wfcStatus FixAllComponent();
wfcStatus GetPosition();

extern "C" int user_initialize(
	int argc,
	char *argv[],
	char *version,
	char *build,
	wchar_t errbuf[80])
{
	OPENEXCEPTIONFILE("w");
	GetDllPath(DllPath);
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		wfcWSession_ptr wSession = wfcWSession::cast(Session);

		try
		{
			pfcUICommand_ptr AutoIndex_com = wSession->UICreateCommand("AutoIndex_com", new MycallbackClass(AutoIndex));
			AutoIndex_com->Designate(MSGFILE, "AutoIndex", "AutoIndexTips", "AutoIndexDescr");
			pfcUICommand_ptr CleanIndex_com = wSession->UICreateCommand("CleanIndex_com", new MycallbackClass(CleanIndex));
			CleanIndex_com->Designate(MSGFILE, "CleanIndex", "CleanIndexTips", "CleanIndexDescr");
			pfcUICommand_ptr VerticalAlign_com = wSession->UICreateCommand("VerticalAlign_com", new MycallbackClass(VerticalAlign));
			VerticalAlign_com->Designate(MSGFILE, "VerticalAlign", "VerticalAlign", "VerticalAlign");
			pfcUICommand_ptr HorizonAlign_com = wSession->UICreateCommand("HorizonAlign_com", new MycallbackClass(HorizonAlign));
			HorizonAlign_com->Designate(MSGFILE, "HorizonAlign", "HorizonAlign", "HorizonAlign");
			pfcUICommand_ptr VDistribution_com = wSession->UICreateCommand("VDistribution_com", new MycallbackClass(VDistribution));
			VDistribution_com->Designate(MSGFILE, "VDistribution", "VDistribution", "VDistribution");
			pfcUICommand_ptr HDistribution_com = wSession->UICreateCommand("HDistribution_com", new MycallbackClass(HDistribution));
			HDistribution_com->Designate(MSGFILE, "HDistribution", "HDistribution", "HDistribution");
			pfcUICommand_ptr SetPad_com = wSession->UICreateCommand("SetPad_com", new MycallbackClass(SetPad));
			SetPad_com->Designate(MSGFILE, "SetPad", "SetPad", "SetPad");
			pfcUICommand_ptr SetDistance_com = wSession->UICreateCommand("SetDistance_com", new MycallbackClass(SetDistance));
			SetDistance_com->Designate(MSGFILE, "SetDistance", "SetDistance", "SetDistance");
			pfcUICommand_ptr CheckIndex_com = wSession->UICreateCommand("CheckIndex_com", new MycallbackClass(CheckIndex));
			CheckIndex_com->Designate(MSGFILE, "CheckIndex", "CheckIndex", "CheckIndex");
			pfcUICommand_ptr Test_com = wSession->UICreateCommand("Test_com", new MycallbackClass(Test));
			Test_com->Designate(MSGFILE, "Test", "Test", "Test");
			pfcUICommand_ptr SetSymDir_Com = wSession->UICreateCommand("SetSymDir_Com", new MycallbackClass(SetSymDir));
			SetSymDir_Com->Designate(MSGFILE, "SetSymDir", "SetSymDir", "SetSymDir");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		try
		{
			pfcUICommand_ptr AddPlasticTechReq_com = wSession->UICreateCommand("AddPlasticTechReq_com", new MycallbackClass(AddPlasticTechReq));
			AddPlasticTechReq_com->Designate(MSGFILE, "AddPlascitTechReq", "AddPlascitTechReq", "AddPlascitTechReq");
			pfcUICommand_ptr AddSheetmetalTechReq_com = wSession->UICreateCommand("AddSheetmetalTechReq_com", new MycallbackClass(AddSheetmetalTechReq));
			AddSheetmetalTechReq_com->Designate(MSGFILE, "AddSheetmetalTechReq", "AddSheetmetalTechReq", "AddSheetmetalTechReq");
			pfcUICommand_ptr AddCastingTechReq_com = wSession->UICreateCommand("AddCastingTechReq_com", new MycallbackClass(AddCastingTechReq));
			AddCastingTechReq_com->Designate(MSGFILE, "AddCastingTechReq", "AddCastingTechReq", "AddCastingTechReq");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		try
		{
			pfcUICommand_ptr AddCenterLine_com = wSession->UICreateCommand("AddCenterLine_com", new MycallbackClass(AddCenterLine));
			AddCenterLine_com->Designate(MSGFILE, "AddCenterLine", "AddCenterLineTips", "AddCenterLine");
			pfcUICommand_ptr SetLength_com = wSession->UICreateCommand("SetLength_com", new MycallbackClass(SetLength));
			SetLength_com->Designate(MSGFILE, "SetLength", "SetLengthTips", "SetLength");
			pfcUICommand_ptr SetAttribute_com = wSession->UICreateCommand("SetAttribute_com", new MycallbackClass(SetAttribute));
			SetAttribute_com->Designate(MSGFILE, "SetAttribute", "SetAttributeTips", "SetAttribute");
			pfcUICommand_ptr JoinLine_com = wSession->UICreateCommand("JoinLine_com", new MycallbackClass(JoinLine));
			JoinLine_com->Designate(MSGFILE, "JoinLine", "JoinLine", "JoinLine");
			pfcUICommand_ptr AddSymmetryLine_com = wSession->UICreateCommand("AddSymmetryLine_com", new MycallbackClass(AddSymmetryLine));
			AddSymmetryLine_com->Designate(MSGFILE, "AddSymmetryLine", "AddSymmetryLine", "AddSymmetryLine");
			pfcUICommand_ptr AddCenterLine2P_com = wSession->UICreateCommand("AddCenterLine2P_com", new MycallbackClass(AddCenterLine2P));
			AddCenterLine2P_com->Designate(MSGFILE, "AddCenterLine2P", "AddCenterLine2P", "AddCenterLine2P");
			pfcUICommand_ptr ExtendLine_com = wSession->UICreateCommand("ExtendLine_com", new MycallbackClass(ExtendLine));
			ExtendLine_com->Designate(MSGFILE, "ExtendLine", "ExtendLine", "ExtendLine");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		try
		{
			pfcUICommand_ptr ViewTableInfo_com = wSession->UICreateCommand("ViewTableInfo_com", new MycallbackClass(ViewTableInfo));
			ViewTableInfo_com->Designate(MSGFILE, "ViewTableInfo", "ViewTableInfo", "ViewTableInfo");
			pfcUICommand_ptr ChangeTable_com = wSession->UICreateCommand("ChangeTable_com", new MycallbackClass(ChangeTable));
			ChangeTable_com->Designate(MSGFILE, "ChangeTable", "ChangeTable", "ChangeTable");
			pfcUICommand_ptr GetCornerCoor_com = wSession->UICreateCommand("GetCornerCoor_com", new MycallbackClass(GetCornerCoor));
			GetCornerCoor_com->Designate(MSGFILE, "GetCornerCoor", "GetCornerCoor", "GetCornerCoor");
			pfcUICommand_ptr CreateTable_com = wSession->UICreateCommand("CreateTable_com", new MycallbackClass(CreateTable));
			CreateTable_com->Designate(MSGFILE, "CreateTable", "CreateTable", "CreateTable");
			Session->SetConfigOption("mapkey", "o48 ~ Command `CreateTable_com`;");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		try
		{
			pfcUICommand_ptr OpenDrawing_com = wSession->UICreateCommand("OpenDrawing_com", new MycallbackClass(OpenDrawing));
			OpenDrawing_com->Designate(MSGFILE, "OpenDrawing", "OpenDrawing", "OpenDrawing");
			pfcUICommand_ptr MoveDrawing_com = wSession->UICreateCommand("MoveDrawing_com", new MycallbackClass(MoveDrawing));
			MoveDrawing_com->Designate(MSGFILE, "MoveDrawing", "MoveDrawing", "MoveDrawing");
			pfcUICommand_ptr Drw2Pdf_com = wSession->UICreateCommand("Drw2Pdf_com", new MycallbackClass(Drw2Pdf));
			Drw2Pdf_com->Designate(MSGFILE, "Drw2Pdf", "Drw2Pdf", "Drw2Pdf");
			pfcUICommand_ptr Export2Stp_com = wSession->UICreateCommand("Export2Stp_com", new MycallbackClass(Export2Stp));
			Export2Stp_com->Designate(MSGFILE, "Export2Stp", "Export2Stp", "Export2Stp");
			pfcUICommand_ptr FixAllComponent_com = wSession->UICreateCommand("FixAllComponent_com", new MycallbackClass(FixAllComponent));
			FixAllComponent_com->Designate(MSGFILE, "FixAllComponent", "FixAllComponent", "FixAllComponent");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		try
		{
			pfcUICommand_ptr GetPosition_com = wSession->UICreateCommand("GetPosition_com", new MycallbackClass(GetPosition));
			GetPosition_com->Designate(MSGFILE, "GetPosition", "GetPosition", "GetPosition");
			pfcUICommand_ptr ShowDllPath_com = wSession->UICreateCommand("ShowDllPath_com", new MycallbackClass(ShowDllPath));
			ShowDllPath_com->Designate(MSGFILE, "ShowDllPath", "ShowDllPath", "ShowDllPath");
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		catch (exception e)
		{
			fprintf(exception_info, "Create Command filed! Err:%s\n", e.what());
		}
		fclose(exception_info);
		return 0;
	}
	catch (...)
	{
		fputs("Initialize filed!\n", exception_info);
	}
	fclose(exception_info);
	return (int)0;
}

extern "C" void user_terminate()
{
}

int main(int argc, char **argv)
{
	if (argc > 2 && strcmp(argv[1], "-cip") == 0)
	{
		int testnum = atoi(argv[2]);
		return 0;
	}

	wfcOtkMain(argc, argv);
	return 0;
}
