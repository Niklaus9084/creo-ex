#ifndef PLATFORM
#define PLATFORM X86E_WIN64
#endif
#ifdef UNICODE
#undef UNICODE
#endif

#include "OTKXMain.cxx"
#include "OTKXDimension.cxx"
#include "OTKXString.cpp"
#include "OTKXAssembly.cpp"
#include "OTKXListener.cxx"
