#include <OTKXMain.h>

wfcStatus AddTechReq(regex begin, regex end)
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	fstream TechReq;
	string TechReqDir = DllPath + "text\\����Ҫ��.txt";
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());

		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcModel2D_ptr mdl2d = pfcModel2D::cast(mdl);
		pfcDetailTextLines_ptr TextLines = pfcDetailTextLines::create();
		try
		{
			TechReq.open(TechReqDir, ios::in | ios::beg);
			string line;
			cStringT lines[20] = {"end"};
			smatch result;
			if (TechReq.is_open())
			{
				while (!TechReq.eof())
				{
					getline(TechReq, line);
					if (regex_match(line, result, begin))
					{
						break;
					}
				}
				while (!TechReq.eof())
				{
					getline(TechReq, line);
					if (regex_match(line, result, end))
						break;
					pfcDetailText_ptr DetailText = pfcDetailText::Create(line.c_str());
					DetailText->SetTextHeight(NULL);
					pfcDetailTexts_ptr DetailTexts = pfcDetailTexts::create();
					DetailTexts->append(DetailText);
					pfcDetailTextLine_ptr TextLine = pfcDetailTextLine::Create(DetailTexts);
					TextLines->append(TextLine);
				}
			}
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		catch (...)
		{
			fputs("Create TextLines Failed!\n", exception_info);
		}
		pfcDetailNoteInstructions_ptr DetailNoteInstructions = pfcDetailNoteInstructions::Create(TextLines);
		session->UIDisplayLocalizedMessage(MSGFILE, "Click Mouse Left Button...", NULL);
		pfcPoint3D_ptr Point3D = session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
		session->UIClearMessage();
		pfcDetailNoteInstructions_ptr NoteInst = pfcDetailNoteInstructions::Create(TextLines);
		pfcAttachments_ptr Attachments = pfcAttachments::create();
		pfcFreeAttachment_ptr FreeAttachment = pfcFreeAttachment::Create(Point3D);
		pfcDetailLeaders_ptr DetailLeaders = pfcDetailLeaders::Create();
		DetailLeaders->SetItemAttachment((pfcAttachment_ptr)FreeAttachment);
		NoteInst->SetLeader(DetailLeaders);
		pfcDetailCreateInstructions_ptr DetailInst = (pfcDetailCreateInstructions_ptr)NoteInst;
		pfcDetailNoteItem_ptr DetailNoteItem = pfcDetailNoteItem::cast(mdl2d->CreateDetailItem(DetailInst));
		DetailNoteItem->Show();
		mdl2d->Regenerate();
		session->UIDisplayLocalizedMessage(MSGFILE, "Add Tech Req Done!", NULL);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Add TechReq Failed!\n", exception_info);
	}
	return wfcTK_NO_ERROR;
}
wfcStatus AddPlasticTechReq()
{
	regex begin(".*<plastic>.*", regex::icase);
	regex end(".*</plastic>.*", regex::icase);
	return AddTechReq(begin, end);
}
wfcStatus AddSheetmetalTechReq()
{
	regex begin(".*<sheet metal>.*", regex::icase);
	regex end(".*</sheet metal>.*", regex::icase);
	return AddTechReq(begin, end);
}
wfcStatus AddCastingTechReq()
{
	regex begin(".*<casting>.*", regex::icase);
	regex end(".*</casting>.*", regex::icase);
	return AddTechReq(begin, end);
}