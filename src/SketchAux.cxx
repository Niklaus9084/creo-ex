#include <OTKXMain.h>
#define Max_Lines_Num 200
int CenterLineLength = 5;
xstring FontName = "CTRLFONT_MID_L";
pfcColorRGB_ptr LineColor = pfcColorRGB::Create(1, 0, 0);
xreal LineWidth = 0;
void CreateLine(pfcDrawing_ptr drawing, pfcView2D_ptr View2D, pfcPoint3D_ptr begin, pfcPoint3D_ptr end, pfcDetailItem_ptr *DetailItem)
{
	begin->set(2, 0);
	end->set(2, 0);
	pfcLineDescriptor_ptr LineDescr = pfcLineDescriptor::Create(begin, end);
	pfcDetailEntityInstructions_ptr EntityInst = pfcDetailEntityInstructions::Create((pfcCurveDescriptor_ptr)LineDescr, View2D);
	EntityInst->SetFontName(FontName);
	EntityInst->SetColor(LineColor);
	EntityInst->SetWidth(LineWidth);
	*DetailItem = drawing->CreateDetailItem((pfcDetailCreateInstructions_ptr)EntityInst);
}
void CreateCenterLine(pfcDrawing_ptr drawing, pfcView2D_ptr View2D, pfcPoint3D_ptr CurveCenter, xreal CurveRadius)
{
	pfcPoint3D_ptr end1 = pfcPoint3D::create();
	pfcPoint3D_ptr end2 = pfcPoint3D::create();
	pfcDetailItem_ptr DetailItem;
	int LineLength = ceil((CurveRadius + CenterLineLength) / 5) * 5;
	end1->set(0, CurveCenter->get(0) - LineLength);
	end1->set(1, CurveCenter->get(1));
	end2->set(0, CurveCenter->get(0) + LineLength);
	end2->set(1, CurveCenter->get(1));
	CreateLine(drawing, View2D, end1, end2, &DetailItem);
	end1->set(1, CurveCenter->get(1) - LineLength);
	end1->set(0, CurveCenter->get(0));
	end2->set(1, CurveCenter->get(1) + LineLength);
	end2->set(0, CurveCenter->get(0));
	CreateLine(drawing, View2D, end1, end2, &DetailItem);
}
wfcStatus AddCenterLine()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		pfcSelectionBuffer_ptr SelectionBuffer = Session->GetCurrentSelectionBuffer();
		pfcSelections_ptr Sels = SelectionBuffer->GetContents();
		pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("edge");
		SelectionOpts->SetMaxNumSels(-1);
		while (true)
		{
			pfcSelections_ptr Selections = Session->Select(SelectionOpts, Sels);
			Sels = NULL;
			if (Selections != NULL)
			{
				xint SelectNum = Selections->getarraysize();
				for (xint i = 0; i < SelectNum; i++)
				{
					pfcSelection_ptr Selection = Selections->get(i);
					pfcModelItem_ptr ModelItem = Selection->GetSelItem();
					pfcView2D_ptr View2D = Selection->GetSelView2D();
					pfcEdge_ptr Edge;
					if (ModelItem->GetType() == pfcITEM_EDGE)
						Edge = pfcEdge::cast(ModelItem);
					else
						continue;
					pfcCurveDescriptor_ptr CurveDescr = Edge->GetCurveDescriptor();
					pfcCurveType CurveType = CurveDescr->GetCurveType();
					pfcPoint3D_ptr CurveCenter;
					xreal Radius;
					if (CurveType == pfcCURVE_ARC)
					{
						pfcArcDescriptor_ptr ArcDescr = pfcArcDescriptor::cast(CurveDescr);
						CurveCenter = ArcDescr->GetCenter();
						Radius = ArcDescr->GetRadius();
					}
					else if (CurveType == pfcCURVE_CIRCLE)
					{
						pfcCircleDescriptor_ptr CircleDescr = pfcCircleDescriptor::cast(CurveDescr);
						CurveCenter = CircleDescr->GetCenter();
						Radius = CircleDescr->GetRadius();
					}
					else
						continue;
					pfcTransform3D_ptr Transform3D = View2D->GetTransform();
					pfcFeature_ptr Feature = Edge->GetFeature();
					wfcWFeature_ptr wFeature = wfcWFeature::cast(Feature);
					pfcSolid_ptr Solid = wFeature->GetSolid();
					wfcAssemblyNameRule_ptr NameRule = wfcAssemblyNameRule::Create(Solid->GetFullName());
					pfcModel_ptr DrwSolid = Drawing->GetCurrentSolid();
					//装配图中零件坐标需转到装配坐标
					if (DrwSolid->GetType() == pfcMDL_ASSEMBLY)
					{
						pfcAssembly_ptr Assembly = pfcAssembly::cast(DrwSolid);
						wfcWAssembly_ptr wAssembly = wfcWAssembly::cast(Assembly);
						wfcWComponentPaths_ptr WComponentPaths = wAssembly->ListComponentsByAssemblyRule((wfcAssemblyRule_ptr)NameRule);
						if (WComponentPaths != NULL)
						{
							wfcWComponentPath_ptr wComponentPath = WComponentPaths->get(0);
							pfcTransform3D_ptr Component_Trf = wComponentPath->GetTransform(true); //request the transformation from the model to the assembly
							CurveCenter = Component_Trf->TransformPoint(CurveCenter);
						}
					}
					CreateCenterLine(Drawing, View2D, Transform3D->TransformPoint(CurveCenter), Radius);
				}
				Drawing->Regenerate();
			}
			else
			{
				fclose(exception_info);
				return wfcTK_NO_ERROR;
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Add CenterLine Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus SetLength()
{
	pfcSession_ptr Session = pfcGetProESession();
	Session->UIDisplayLocalizedMessage(MSGFILE, "Please Input CenterLineLength:", NULL);
	CenterLineLength = Session->UIReadIntMessage(0, 100);
	return wfcTK_NO_ERROR;
}
wfcStatus SetAttribute()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("draft_ent");
		SelectionOpts->SetMaxNumSels(1);
		pfcSelections_ptr Selections = Session->Select(SelectionOpts);
		if (Selections != NULL)
		{
			pfcModelItem_ptr ModelItem = Selections->get(0)->GetSelItem();
			pfcDetailEntityItem_ptr DetailEntityItem = pfcDetailEntityItem::cast(ModelItem);
			pfcDetailEntityInstructions_ptr DetailEntityInst = DetailEntityItem->GetInstructions();
			FontName = DetailEntityInst->GetFontName();
			LineColor = DetailEntityInst->GetColor();
			LineWidth = DetailEntityInst->GetWidth();
		}
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
void _JoinLine(pfcDrawing_ptr drawing, pfcView2D_ptr View2D, CrLine lines[])
{
	OPENEXCEPTIONFILE("a");
	try
	{
		for (int i = 0; i < Max_Lines_Num - 1; i++)
		{
			if (lines[i].DetailItem == NULL)
				continue;
			for (int j = i + 1; j < Max_Lines_Num; j++)
			{
				if (lines[j].DetailItem == NULL)
					continue;
				//判断两条直线是否重合
				bool joinable = false;
				//判断两条直线竖直且重合
				if (abs(lines[i].GetStart()->get(0) - lines[i].GetEnd()->get(0)) < eps &&
					abs(lines[j].GetStart()->get(0) - lines[j].GetEnd()->get(0)) < eps &&
					abs(lines[i].GetStart()->get(0) - lines[j].GetStart()->get(0)) < eps)
				{
					joinable = true;
				}
				//判断两条直线非竖直且重合
				else if ((abs(lines[i].GetDirect() - lines[j].GetDirect()) < eps) &&
						 (abs(lines[i].GetOffset() - lines[j].GetOffset()) < eps))
				{
					joinable = true;
				}
				if (joinable)
				{
					try
					{
						pfcPoint3D_ptr points[] = {lines[j].GetStart(), lines[i].GetEnd(), lines[j].GetEnd()};
						pfcPoint3D_ptr start, end;
						start = end = lines[i].GetStart();
						int sortflag = 1;
						//水平直线按x坐标排序，非水平直线按y坐标排序
						if (lines[i].IsHorizon())
						{
							sortflag = 0;
						}
						//找出左下点start 右上点end
						for (int m = 0; m < 3; m++)
						{
							if (points[m]->get(sortflag) < start->get(sortflag))
								start = points[m];
							if (points[m]->get(sortflag) > end->get(sortflag))
								end = points[m];
						}
						lines[i].DetailItem->Delete();
						lines[j].DetailItem->Delete();
						pfcDetailItem_ptr DetailItem;
						CreateLine(drawing, View2D, start, end, &DetailItem);
						lines[i].DetailItem = DetailItem;
						lines[i].Change(start, end);
						lines[j].DetailItem = NULL;
					}
					OTK_EXCEPTION_HANDLER(exception_info);
				}
			}
		}
	}
	catch (exception e)
	{
		fprintf(exception_info, "_JoinLine Failed! Error is:%s\n", e.what());
	}
	fclose(exception_info);
}
//合并直线
wfcStatus JoinLine()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("w");
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		Drawing->Regenerate();
		pfcSelectionBuffer_ptr SelectBuffer = Session->GetCurrentSelectionBuffer();
		pfcSelections_ptr Selections = SelectBuffer->GetContents();
		if (Selections == NULL)
		{
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		xint num = Selections->getarraysize();
		pfcPoint3D_ptr xstart, xend, ystart, yend;
		xstart = xend = ystart = yend = pfcPoint3D::create();
		pfcView2D_ptr View2D = Selections->get(0)->GetSelView2D();
		CrLine Lines[Max_Lines_Num];
		int LineIndex = 0;
		bool SelectedIsValid = true;
		for (xint i = 0; i < num; i++)
		{
			//Selection有效性验证
			//刚绘制的图元在SelectionBuffer中的Selection无效,需先清空Buffer,才能继续选择（猜测）
			try
			{
				wfcWSelection_ptr WSelection = wfcWSelection::cast(Selections->get(i));
				WSelection->Verify();
			}
			xcatchbegin
				xcatch(pfcXToolkitError, pfcex)
			{
				SelectedIsValid = false;
				continue;
			}
			xcatchend
				pfcModelItem_ptr ModelItem = Selections->get(i)->GetSelItem();
			if (ModelItem->GetType() != pfcITEM_DTL_ENTITY)
				continue;
			pfcDetailItem_ptr DetailItem = pfcDetailItem::cast(ModelItem);
			if (DetailItem->GetDetailType() != pfcDETAIL_ENTITY)
				continue;
			pfcDetailEntityItem_ptr DetailEntityItem = pfcDetailEntityItem::cast(DetailItem);
			pfcDetailEntityInstructions_ptr DetailEntityInst = DetailEntityItem->GetInstructions();
			pfcCurveDescriptor_ptr CurveDescr = DetailEntityInst->GetGeometry();
			pfcCurveType CurveType = CurveDescr->GetCurveType();
			if (CurveType == pfcCURVE_LINE)
			{
				pfcPoint3D_ptr lstart, lend;
				pfcLineDescriptor_ptr LineDescr = pfcLineDescriptor::cast(CurveDescr);
				lstart = LineDescr->GetEnd1();
				lend = LineDescr->GetEnd2();
				Lines[LineIndex].DetailItem = DetailItem;
				Lines[LineIndex].Create(lstart, lend);
				LineIndex += 1;
				fclose(exception_info);
			}
		}
		_JoinLine(Drawing, View2D, Lines);
		OPENEXCEPTIONFILE("a");
		Drawing->Regenerate();
		if (!SelectedIsValid)
			Session->UIDisplayMessage(MSGFILE, "The Selected is invalid! Please Choose again.", NULL);
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "JoinLine Failed! Error is:%s\n", e.what());
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus GetPosition()
{
	OPENEXCEPTIONFILE("w");
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		session->UIDisplayLocalizedMessage(MSGFILE, "Click Mouse Left Button...", NULL);
		pfcModel_ptr Model = session->GetCurrentModel();
		if (CheckEnv() == 0)
			return wfcTK_NO_ERROR;
		pfcModel2D_ptr Model2D = pfcModel2D::cast(Model);
		xint sheetNumber = Model2D->GetCurrentSheetNumber();
		pfcTransform3D_ptr Transform3D = Model2D->GetSheetTransform(sheetNumber);
		pfcPoint3D_ptr Point3D;
		xstringsequence_ptr seq = xstringsequence::create();
		pfcMouseStatus_ptr MouseStatus;
		pfcMouseButton MouseButton;
		while (1)
		{
			MouseStatus = session->UIGetCurrentMouseStatus(false);
			Point3D = MouseStatus->GetPosition();
			Point3D = Transform3D->TransformPoint(Point3D);
			MouseButton = MouseStatus->GetSelectedButton();
			if (MouseButton == pfcMOUSE_BTN_LEFT)
				break;
			seq->clear();
			seq->append(to_string(Point3D->get(0)).c_str());
			seq->append(to_string(Point3D->get(1)).c_str());
			seq->append(to_string(Point3D->get(2)).c_str());
			session->UIDisplayMessage(MSGFILE, "X = %0s,Y = %1s,Z = %2s", seq);
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info);
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
//添加对称线
wfcStatus AddSymmetryLine()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		pfcDetailItem_ptr DetailItem;
		pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("edge");
		SelectionOpts->SetMaxNumSels(1);
		while (true)
		{
			pfcSelections_ptr Selections = Session->Select(SelectionOpts, NULL);
			if (Selections != NULL)
			{
				xint SelectNum = Selections->getarraysize();
				for (xint i = 0; i < SelectNum; i++)
				{
					pfcSelection_ptr Selection = Selections->get(i);
					pfcModelItem_ptr ModelItem = Selection->GetSelItem();
					pfcView2D_ptr View2D = Selection->GetSelView2D();
					pfcEdge_ptr Edge;
					if (ModelItem->GetType() == pfcITEM_EDGE)
						Edge = pfcEdge::cast(ModelItem);
					else
						continue;
					pfcCurveDescriptor_ptr CurveDescr = Edge->GetCurveDescriptor();
					pfcCurveType CurveType = CurveDescr->GetCurveType();
					pfcPoint3D_ptr mid = pfcPoint3D::create();
					if (CurveType == pfcCURVE_ARC)
					{
						pfcArcDescriptor_ptr ArcDescr = pfcArcDescriptor::cast(CurveDescr);
						mid = ArcDescr->GetCenter();
					}
					else if (CurveType == pfcCURVE_CIRCLE)
					{
						pfcCircleDescriptor_ptr CircleDescr = pfcCircleDescriptor::cast(CurveDescr);
						mid = CircleDescr->GetCenter();
					}
					else if (CurveType == pfcCURVE_LINE)
					{
						pfcLineDescriptor_ptr LineDescr = pfcLineDescriptor::cast(CurveDescr);
						pfcPoint3D_ptr start, end;
						start = LineDescr->GetEnd1();
						end = LineDescr->GetEnd2();
						for (xint v = 0; v < 3; v++)
						{
							mid->set(v, (start->get(v) + end->get(v)) / 2);
						}
					}
					else
						continue;
					pfcTransform3D_ptr Transform3D = View2D->GetTransform();
					pfcFeature_ptr Feature = Edge->GetFeature();
					wfcWFeature_ptr wFeature = wfcWFeature::cast(Feature);
					pfcSolid_ptr Solid = wFeature->GetSolid();
					wfcAssemblyNameRule_ptr NameRule = wfcAssemblyNameRule::Create(Solid->GetFullName());
					pfcModel_ptr DrwSolid = Drawing->GetCurrentSolid();
					if (DrwSolid->GetType() == pfcMDL_ASSEMBLY)
					{
						pfcAssembly_ptr Assembly = pfcAssembly::cast(DrwSolid);
						wfcWAssembly_ptr wAssembly = wfcWAssembly::cast(Assembly);
						wfcWComponentPaths_ptr WComponentPaths = wAssembly->ListComponentsByAssemblyRule((wfcAssemblyRule_ptr)NameRule);
						if (WComponentPaths != NULL)
						{
							wfcWComponentPath_ptr wComponentPath = WComponentPaths->get(0);
							pfcTransform3D_ptr Component_Trf = wComponentPath->GetTransform(true); //request the transformation from the model to the assembly
							mid = Component_Trf->TransformPoint(mid);
						}
					}
					mid = Transform3D->TransformPoint(mid);
					pfcPoint3D_ptr symstart, symend;
					symstart = Session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
					symend = Session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
					xint dx, dy;
					dx = abs(symstart->get(0) - symend->get(0));
					dy = abs(symstart->get(1) - symend->get(1));
					if (dx > dy)
					{
						symstart->set(1, mid->get(1));
						symend->set(1, mid->get(1));
						CreateLine(Drawing, View2D, symstart, symend, &DetailItem);
					}
					else
					{
						symstart->set(0, mid->get(0));
						symend->set(0, mid->get(0));
						CreateLine(Drawing, View2D, symstart, symend, &DetailItem);
					}
				}
				Drawing->Regenerate();
			}
			else
			{
				fclose(exception_info);
				return wfcTK_NO_ERROR;
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Add CenterLine Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
//添加两点中心线
wfcStatus AddCenterLine2P()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		pfcDetailItem_ptr DetailItem;
		pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("edge");
		SelectionOpts->SetMaxNumSels(2);
		while (true)
		{
			pfcSelections_ptr Selections = Session->Select(SelectionOpts, NULL);
			if (Selections != NULL)
			{
				xint SelectNum = Selections->getarraysize();
				pfcView2D_ptr View2D = Selections->get(0)->GetSelView2D();
				pfcPoint3Ds_ptr mids = pfcPoint3Ds::create();
				for (xint i = 0; i < SelectNum; i++)
				{
					pfcSelection_ptr Selection = Selections->get(i);
					pfcModelItem_ptr ModelItem = Selection->GetSelItem();
					pfcEdge_ptr Edge;
					if (ModelItem->GetType() == pfcITEM_EDGE)
						Edge = pfcEdge::cast(ModelItem);
					else
						continue;
					pfcCurveDescriptor_ptr CurveDescr = Edge->GetCurveDescriptor();
					pfcCurveType CurveType = CurveDescr->GetCurveType();
					pfcPoint3D_ptr mid;
					if (CurveType == pfcCURVE_ARC)
					{
						pfcArcDescriptor_ptr ArcDescr = pfcArcDescriptor::cast(CurveDescr);
						mid = ArcDescr->GetCenter();
					}
					else if (CurveType == pfcCURVE_CIRCLE)
					{
						pfcCircleDescriptor_ptr CircleDescr = pfcCircleDescriptor::cast(CurveDescr);
						mid = CircleDescr->GetCenter();
					}
					else
						continue;
					pfcTransform3D_ptr Transform3D = View2D->GetTransform();
					pfcFeature_ptr Feature = Edge->GetFeature();
					wfcWFeature_ptr wFeature = wfcWFeature::cast(Feature);
					pfcSolid_ptr Solid = wFeature->GetSolid();
					wfcAssemblyNameRule_ptr NameRule = wfcAssemblyNameRule::Create(Solid->GetFullName());
					pfcModel_ptr DrwSolid = Drawing->GetCurrentSolid();
					if (DrwSolid->GetType() == pfcMDL_ASSEMBLY)
					{
						pfcAssembly_ptr Assembly = pfcAssembly::cast(DrwSolid);
						wfcWAssembly_ptr wAssembly = wfcWAssembly::cast(Assembly);
						wfcWComponentPaths_ptr WComponentPaths = wAssembly->ListComponentsByAssemblyRule((wfcAssemblyRule_ptr)NameRule);
						if (WComponentPaths != NULL)
						{
							wfcWComponentPath_ptr wComponentPath = WComponentPaths->get(0);
							pfcTransform3D_ptr Component_Trf = wComponentPath->GetTransform(true); //request the transformation from the model to the assembly
							mid = Component_Trf->TransformPoint(mid);
						}
					}
					mid = Transform3D->TransformPoint(mid);
					mids->append(mid);
				}
				if (mids->getarraysize() == 2)
				{
					CreateLine(Drawing, View2D, mids->get(0), mids->get(1), &DetailItem);
					Drawing->Regenerate();
				}
				else
					continue;
			}
			else
			{
				fclose(exception_info);
				return wfcTK_NO_ERROR;
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Add CenterLine Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus ExtendLine()
{
	OPENEXCEPTIONFILE("a");
	try
	{
		if (CheckEnv() == 0)
		{
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model = Session->GetCurrentModel();
		pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
		pfcDetailItem_ptr DetailItem;
		pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("draft_ent");
		SelectionOpts->SetMaxNumSels(1);
		while (true)
		{
			pfcSelections_ptr Selections = Session->Select(SelectionOpts, NULL);
			if (Selections != NULL)
			{
				xint SelectNum = Selections->getarraysize();
				pfcView2D_ptr View2D = Selections->get(0)->GetSelView2D();
				pfcPoint3Ds_ptr mids = pfcPoint3Ds::create();
				for (xint i = 0; i < SelectNum; i++)
				{
					pfcSelection_ptr Selection = Selections->get(i);
					pfcModelItem_ptr ModelItem = Selection->GetSelItem();
					pfcDetailItem_ptr DetailItem;
					pfcDetailEntityItem_ptr DetailEntity;
					if (ModelItem->GetType() == pfcITEM_DTL_ENTITY)
					{
						DetailItem = pfcDetailItem::cast(ModelItem);
						if (DetailItem->GetDetailType() == pfcDETAIL_ENTITY)
						{
							DetailEntity = pfcDetailEntityItem::cast(DetailItem);
						}
					}
					else
						continue;
					pfcDetailEntityInstructions_ptr DetailEntityInst;
					DetailEntityInst = DetailEntity->GetInstructions();
					pfcCurveDescriptor_ptr CurveDescr = DetailEntityInst->GetGeometry();
					pfcCurveType CurveType = CurveDescr->GetCurveType();
					if (CurveType == pfcCURVE_LINE)
					{
						pfcPoint3D_ptr aim;
						aim = Session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
						pfcLineDescriptor_ptr LineDescr = pfcLineDescriptor::cast(CurveDescr);
						CrLine CurrentLine;
						CurrentLine.Create(LineDescr->GetEnd1(), LineDescr->GetEnd2());
						CurrentLine.ExtendToPoint(aim);
						pfcPoint3D_ptr start, end;
						start = CurrentLine.GetStart();
						end = CurrentLine.GetEnd();
						LineDescr->SetEnd1(CurrentLine.GetStart());
						LineDescr->SetEnd2(CurrentLine.GetEnd());
						DetailEntityInst->SetGeometry((pfcCurveDescriptor_ptr)LineDescr);
						DetailEntity->Modify(DetailEntityInst);
						Drawing->Regenerate();
					}
					else
						continue;
				}
			}
			else
			{
				fclose(exception_info);
				return wfcTK_NO_ERROR;
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Extend CenterLine Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}