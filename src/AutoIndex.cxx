#include <OTKXMain.h>

string SymbolDir;
bool SymDirSetStatus = false;

//收集尺寸 放入链表
void CollectDimension(pfcModel_ptr mdl, DimensionList &Dimlist)
{
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcPoint3D_ptr loc;
		pfcDrawing_ptr drawing = pfcDrawing::cast(mdl);
		pfcDimension2Ds_ptr Dimensions = pfcDimension2Ds::create();
		pfcModels_ptr Models = drawing->ListModels();
		//获取组件图中的尺寸
		for (xint i = 0; i < Models->getarraysize(); i++)
		{
			Dimensions->insertseq(0, drawing->ListShownDimensions(Models->get(i), pfcModelItemType_nil));
		}
		//获取当前模型中尺寸
		pfcModelItems_ptr mdlitems = mdl->ListItems(pfcITEM_DIMENSION);
		xint numitems = mdlitems->getarraysize();
		for (xint i = 0; i < numitems; i++)
		{
			pfcModelItem_ptr mdlitem = mdlitems->get(i);
			pfcDimension2D_ptr dim = pfcDimension2D::cast(mdlitem);
			Dimensions->append(dim);
		}
		//根据Dimensions创建DimStruct链表，便于后续操作
		for (xint i = 0; i < Dimensions->getarraysize(); i++)
		{

			pfcDimension2D_ptr dim = Dimensions->get(i);
			DimensionStruct DimStruct;
			//DimStruct成员变量在初始化时创建
			DimStruct.Create(dim);
			Dimlist.append_tail(DimStruct);
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Collect Dimension Failed!\n", exception_info);
	}
	fclose(exception_info);
}
//尺寸排序 视图自左而右 自上而下  视图内逆时针排序
void SortDimensionList(DimensionList &Dimlist)
{
	OPENEXCEPTIONFILE("a");
	try
	{
		int listsize = Dimlist.size();
		//先按角度和距离排序
		for (int i = 0; i < listsize - 1; i++)
		{
			for (int j = 0; j < listsize - i - 1; j++)
			{

				double angle1, angle2;
				double dist1, dist2;
				angle1 = Dimlist.get(j).GetAngle();
				angle2 = Dimlist.get(j + 1).GetAngle();
				dist1 = Dimlist.get(j).GetDistance();
				dist2 = Dimlist.get(j + 1).GetDistance();
				if (angle1 > angle2)
				{
					Dimlist.swap(j, j + 1);
				}
				else if (angle1 == angle2 && dist1 < dist2)
				{
					Dimlist.swap(j, j + 1);
				}
			}
		}
		//按视图位置排序————当视图为局部视图时，视图中心偏移，虽然视图时对齐的，但中心相对位置不确定
		for (int i = 0; i < listsize - 1; i++)
		{
			for (int j = 0; j < listsize - i - 1; j++)
			{
				float vcx1, vcx2, vcy1, vcy2;
				vcx1 = Dimlist.get(j).GetViewCenter()->get(0);
				vcx2 = Dimlist.get(j + 1).GetViewCenter()->get(0);
				vcy1 = Dimlist.get(j).GetViewCenter()->get(1);
				vcy2 = Dimlist.get(j + 1).GetViewCenter()->get(1);
				if (vcx1 > vcx2)
				{
					Dimlist.swap(j, j + 1);
				}
				else if ((vcx1 == vcx2) && (vcy1 < vcy2))
				{
					Dimlist.swap(j, j + 1);
				}
			}
		}
	}
	catch (...)
	{
		fputs("Sort Dimension List Failed!\n", exception_info);
	}
	fclose(exception_info);
}
pfcDetailSymbolInstInstructions_ptr CreateSymbolInstInstruction(pfcModel_ptr mdl)
{
	if (!SymDirSetStatus)
		SymbolDir = DllPath + "symbol";
	pfcModel2D_ptr mdl2D = pfcModel2D::cast(mdl);
	pfcDetailSymbolDefItem_ptr symdef = ((pfcDetailItemOwner_ptr)mdl2D)->RetrieveSymbolDefinition("boolean", SymbolDir.c_str(), NULL, true);
	pfcDetailSymbolInstInstructions_ptr symins = pfcDetailSymbolInstInstructions::Create(symdef);
	return symins;
}

void CreateSymbolIndex(pfcDrawing_ptr drawing,
					   pfcDimension2D_ptr dimension,
					   pfcPoint3D_ptr loc,
					   pfcDetailSymbolInstInstructions_ptr symins,
					   xint index)
{
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcPoint3D_ptr symloc = loc;
		symloc->set(0, loc->get(0));
		symloc->set(1, loc->get(1));
		stringstream symindex;
		symindex << index + 1;
		pfcDetailVariantText_ptr textvalue = pfcDetailVariantText::Create("index", symindex);
		pfcDetailVariantTexts_ptr textvalues = pfcDetailVariantTexts::create();
		textvalues->append(textvalue);
		symins->SetTextValues(textvalues);
		pfcSymbolDefAttachment_ptr defattachment = pfcSymbolDefAttachment::Create(pfcSYMDEFATTACH_FREE, symloc);
		symins->SetDefAttachment(defattachment);
		pfcSelection_ptr selection = pfcCreateModelItemSelection((pfcModelItem_ptr)dimension);
		pfcOffsetAttachment_ptr attachment = pfcOffsetAttachment::Create(selection, symloc);
		pfcDetailLeaders_ptr leaders = pfcDetailLeaders::Create();
		leaders->SetItemAttachment((pfcAttachment_ptr)attachment);
		symins->SetInstAttachment(leaders);
		symins->SetIsDisplayed(true);
		symins->SetScaledHeight(3);
		((pfcDetailItemOwner_ptr)drawing)->CreateDetailItem((pfcDetailCreateInstructions_ptr)symins);
		textvalues->clear(); //不清空会有异常
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Create Symbol Index Error!\n", exception_info);
	}
	fclose(exception_info);
}
//收集球标
int CollectSymbols(pfcDetailItems_ptr &DetailItems)
{
	if (CheckEnv() == 0) //默认检查是否为绘图环境
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcModel2D_ptr mdl2d = pfcModel2D::cast(mdl);
		DetailItems = mdl2d->ListDetailItems(pfcDETAIL_SYM_INSTANCE, NULL);
		if (DetailItems == NULL)
		{
			session->UIDisplayLocalizedMessage(MSGFILE, "Not Find Boolean", NULL);
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		else
			return 1;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Collect Symbols Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus AutoIndex()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	pfcPoint3D_ptr loc;
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcDrawing_ptr drawing = pfcDrawing::cast(mdl);
		xint sheetNumber = drawing->GetCurrentSheetNumber();
		pfcTransform3D_ptr SheetTrans; //屏幕坐标与图纸坐标转换矩阵
		DimensionList dimlist;
		CollectDimension(mdl, dimlist);
		SortDimensionList(dimlist);
		pfcDetailSymbolInstInstructions_ptr symins = CreateSymbolInstInstruction(mdl);
		int listsize = dimlist.size();
		if (dimlist.is_empty() == true)
		{
			session->UIDisplayLocalizedMessage(MSGFILE, "Dimension is not found!", NULL);
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		try
		{
			// smatch result;
			// regex pattern("\\d+:");
			for (xint i = 0; i < listsize; i++)
			{
				DimensionStruct dimstruct = dimlist.get(i);
				pfcDimension2D_ptr dimension = dimstruct.GetDimension();
				int DimWidth = CalDimensionValueWideth(dimension);
				//偏移位置补偿 计算的字符宽度加1.5pad间隙/2即为球标到尺寸中心距离
				int tpad = ceil((DimWidth + 1.5 * pad) / 2);
				loc = dimension->GetLocation();
				if ((dimension->GetDimType() == pfcDIM_LINEAR))
				{
					pfcPoint3Ds_ptr Points = pfcPoint3Ds::create();
					pfcView2D_ptr view = dimension->GetView();
					xreal viewscal = view->GetScale();
					SetDimensionMid(dimension, Points); //线性尺寸居中
					xreal dimvalue, dx, dy;
					dimvalue = dimension->GetDimValue();
					dx = abs(Points->get(0)->get(0) - Points->get(1)->get(0));
					dy = abs(Points->get(0)->get(1) - Points->get(1)->get(1));
					loc = dimension->GetLocation();
					if (abs(dimvalue * viewscal - dy) < eps) //连接点纵坐标差值与尺寸数值相等，为竖直尺寸
					{
						loc->set(0, loc->get(0) - 2);
						loc->set(1, loc->get(1) + tpad);
					}
					else if (abs(dimvalue * viewscal - dx) < eps) //连接点横坐标差值与尺寸数值相等，为水平尺寸
					{
						loc->set(1, loc->get(1) + 2);
						loc->set(0, loc->get(0) + tpad);
					}
					else //斜向尺寸 目前没有方法计算尺寸方向 球标放置在尺寸左上角
					{
						loc->set(0, loc->get(0) - pad);
						loc->set(1, loc->get(1) + pad);
					}
				}
				else if (dimension->GetDimType() == pfcDIM_DIAMETER) //直径尺寸标注文本为水平方向
				{
					loc->set(1, loc->get(1) + 2);
					loc->set(0, loc->get(0) + tpad);
				}
				else //其余尺寸球标放置在左上角
				{
					loc->set(0, loc->get(0) - pad);
					loc->set(1, loc->get(1) + pad);
				}
				CreateSymbolIndex(drawing, dimension, loc, symins, i);
			}
			drawing->Regenerate();
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		catch (exception e)
		{
			fprintf(exception_info, "CreateSymbolIndex Failed! Error is :%s\n", e.what());
		}
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Autoindex Failed! Error:%s\n", e.what());
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus CleanIndex()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcDetailItems_ptr items;
		pfcModel2D_ptr mdl2d = pfcModel2D::cast(mdl);
		if (!CollectSymbols(items))
			return wfcTK_NO_ERROR;
		xint numitems = items->getarraysize();
		for (xint i = 0; i < numitems; i++)
		{
			pfcDetailItem_ptr detailitem = items->get(i);
			pfcDetailSymbolInstItem_ptr syminst = pfcDetailSymbolInstItem::cast(detailitem);
			pfcDetailSymbolInstInstructions_ptr syminstrut = syminst->GetInstructions(true);
			pfcDetailSymbolDefItem_ptr defsym = syminstrut->GetSymbolDef();
			pfcDetailSymbolDefInstructions_ptr defsymins = defsym->GetInstructions();
			xstring symname = defsymins->GetName();
			if (symname == "BOOLEAN")
			{
				detailitem->Delete();
			}
		}
		mdl2d->Regenerate();
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Clean Index Filed! Error is: %s \n", e.what());
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus CheckIndex()
{
	if (CheckEnv() == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcDetailItems_ptr items;
		pfcModel2D_ptr mdl2d = pfcModel2D::cast(mdl);
		if (!CollectSymbols(items))
			return wfcTK_NO_ERROR;
		xint numitems = items->getarraysize();
		SymbolList Symbols;
		int MaxIndex = 0;
		for (xint i = 0; i < numitems; i++)
		{
			pfcDetailItem_ptr detailitem = items->get(i);
			pfcDetailSymbolInstItem_ptr syminst = pfcDetailSymbolInstItem::cast(detailitem);
			pfcDetailSymbolInstInstructions_ptr syminstrut = syminst->GetInstructions(true);
			pfcDetailVariantTexts_ptr VariantTexts = syminstrut->GetTextValues();
			string Value = VariantTexts->get(0)->GetValue();
			//字符串转数字
			istringstream istr(Value);
			int symbolindex;
			istr >> symbolindex;
			//创建SymbolStruct链表
			SymbolStruct Symbolinst;
			Symbolinst.SymbolInst = syminst;
			Symbolinst.index = symbolindex;
			if (symbolindex > MaxIndex)
				MaxIndex = symbolindex;
			Symbols.append_tail(Symbolinst);
		}
		int numSymbols = Symbols.size();
		int indexflag[200] = {0}; //初始化球标状态标志位

		for (int i = 0; i < numSymbols; i++)
		{
			indexflag[Symbols.get(i).index]++;
		}
		string linerepet;
		string linemiss;
		xstringsequence_ptr seqrepet = xstringsequence::create();
		xstringsequence_ptr seqmiss = xstringsequence::create();
		;
		for (int i = 1; i <= MaxIndex; i++)
		{
			if (indexflag[i] > 1)
			{
				linerepet += to_string(i); //数字转字符串
				linerepet += ",";
			}
			else if (indexflag[i] == 0)
			{
				linemiss += to_string(i);
				linemiss += ",";
			}
		}

		if (!linerepet.empty() && linemiss.empty())
		{
			linerepet = linerepet.substr(0, linerepet.length() - 1);
			seqrepet->append(linerepet.c_str());
			session->UIDisplayMessage(MSGFILE, "Repeat Index:%0s", seqrepet);
		}
		else if (!linemiss.empty() && linerepet.empty())
		{
			linemiss = linemiss.substr(0, linemiss.length() - 1);
			seqmiss->append(linemiss.c_str());
			session->UIDisplayMessage(MSGFILE, "Miss Index:%0s", seqmiss);
		}
		else if (!linerepet.empty() && !linemiss.empty())
		{
			linerepet = linerepet.substr(0, linerepet.length() - 1);
			linemiss = linemiss.substr(0, linemiss.length() - 1);
			seqrepet->append(linerepet.c_str());
			seqrepet->append(linemiss.c_str());
			session->UIDisplayMessage(MSGFILE, "Repeat Index:%0s;Miss Index:%1s", seqrepet);
		}
		else
		{
			string linemaxindex = to_string(MaxIndex);
			xstringsequence_ptr seqmaxindex = xstringsequence::create();
			seqmaxindex->append(linemaxindex.c_str());
			session->UIDisplayMessage(MSGFILE, "The total number of symbols:1~%0s", seqmaxindex);
		}
		mdl2d->Regenerate();
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Check Index Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus SetPad()
{
	pfcSession_ptr Session = pfcGetProESession();
	xstringsequence_ptr SeqCurrentPad = xstringsequence::create();
	string CurrentPad = to_string(pad);
	SeqCurrentPad->append(CurrentPad.c_str());
	Session->UIDisplayLocalizedMessage(MSGFILE, "Current pad is %0w.Please Input pad:", SeqCurrentPad);
	pad = Session->UIReadIntMessage(0, 100);
	return wfcTK_NO_ERROR;
}

//设置球标目录
wfcStatus SetSymDir()
{
	pfcSession_ptr Session = pfcGetProESession();
	pfcDirectorySelectionOptions_ptr Options = pfcDirectorySelectionOptions::Create();
	xstring AimPath = SymbolDir.c_str();
	try
	{
		AimPath = Session->UISelectDirectory(Options);
	}
	xcatchbegin
		xcatch(pfcXToolkitError, pfcx)
	{
		return wfcTK_NO_ERROR;
	}
	xcatchend
		SymbolDir = (string)AimPath;
	SymDirSetStatus = true;
	return wfcTK_NO_ERROR;
}
