#include <OTKXMain.h>
xstringsequence_ptr GenCellTexts(BE::BasicExcelWorksheet *ws, int r, int c)
{
    xstringsequence_ptr CellTexts = xstringsequence::create();
    BE::BasicExcelCell *wc = ws->Cell(r, c);
    if (wc->Type() == BE::BasicExcelCell::WSTRING)
    {
        const wchar_t *text = wc->GetWString();
        xstring xrstr(text);
        CellTexts->append(xrstr);
    }
    else if (wc->Type() == BE::BasicExcelCell::STRING)
    {
        const char *text = wc->GetString();
        xstring xrstr(text);
        CellTexts->append(xrstr);
    }
    else if (wc->Type() == BE::BasicExcelCell::INT)
    {
        int value = wc->GetInteger();
        stringstream text;
        text << value;
        xstring xrstr(text);
        CellTexts->append(xrstr);
    }
    else if (wc->Type() == BE::BasicExcelCell::DOUBLE)
    {
        double value = wc->GetDouble();
        stringstream text;
        text << value;
        xstring xrstr(text);
        CellTexts->append(xrstr);
    }
    return CellTexts;
}
wfcStatus ViewTableInfo()
{
    if (CheckEnv() == 0)
    {
        return wfcTK_NO_ERROR;
    }
    OPENEXCEPTIONFILE("a");
    string xlspath = DllPath + "text/table_info.xls";
    try
    {
        pfcSession_ptr Session = pfcGetProESession();
        pfcModel_ptr Model = Session->GetCurrentModel();
        pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
        pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("dwg_table");
        SelectionOpts->SetMaxNumSels(1);
        pfcSelections_ptr Selections = Session->Select(SelectionOpts);
        if (Selections == NULL)
            return wfcTK_NO_ERROR;
        pfcTable_ptr Table = pfcTable::cast(Selections->get(0)->GetSelItem());
        xint ColCount = Table->GetColumnCount();
        xint RowCount = Table->GetRowCount();
        BE::BasicExcel wb;
        wb.New(1); //初始化一个workbook
        BE::BasicExcelWorksheet *ws = wb.GetWorksheet(0);
        for (xint r = 0; r < RowCount; r++)
        {
            for (xint c = 0; c < ColCount; c++)
            {
                pfcTableCell_ptr TableCell = pfcTableCell::Create(r + 1, c + 1);
                xstring text;
                try
                {
                    xstringsequence_ptr texts = Table->GetText(TableCell, pfcDWGTABLE_NORMAL);
                    text = texts->get(0);
                }
                xcatchbegin
                    xcatch(pfcXToolkitError, e) { text = " "; }
                xcatchend;
                ws->Cell(r, c)->Set(text.operator cWStringT());
            }
        }
        wb.SaveAs(xlspath.c_str());
        fclose(exception_info);
        return wfcTK_NO_ERROR;
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (exception e)
    {
        fprintf(exception_info, "Fill in Table Failed! Error is:%s\n", e.what());
    }
    fclose(exception_info);
    return wfcTK_NO_ERROR;
}
//目前修改表格前需先查看表格ViewTableInfo()得到内容文件后修改，再修改表格内容 操作不够连贯
//优化方向：修改表格->读取表格内容并弹出窗口->窗口关闭后修改表格内容
wfcStatus ChangeTable()
{
    OPENEXCEPTIONFILE("w");
    pfcSession_ptr Session = pfcGetProESession();
    pfcFileOpenOptions_ptr FileOpenOpts = pfcFileOpenOptions::Create("*.xls,*.xlsx");
    xstring filepath = Session->UIOpenFile(FileOpenOpts);
    pfcModel_ptr Model = Session->GetCurrentModel();
    pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
    pfcSelectionOptions_ptr SelectionOpts = pfcSelectionOptions::Create("dwg_table");
    SelectionOpts->SetMaxNumSels(1);
    pfcSelections_ptr Selections = Session->Select(SelectionOpts);
    if (Selections == NULL)
        return wfcTK_NO_ERROR;
    pfcTable_ptr Table = pfcTable::cast(Selections->get(0)->GetSelItem());
    fstream table_info;
    string xlspath = DllPath + "text/table_info.xls";
    BE::BasicExcel wb;
    if (wb.Load(filepath.operator cStringT()) == false)
    {
        Session->UIDisplayMessage(MSGFILE, "File is opened!", NULL);
        return wfcTK_NO_ERROR;
    }
    BE::BasicExcelWorksheet *ws = wb.GetWorksheet(0);
    int rcont, ccont;
    rcont = ws->GetTotalRows();
    ccont = ws->GetTotalCols();
    try
    {
        int trcont, tccont;
        trcont = Table->GetRowCount();
        tccont = Table->GetColumnCount();
        pfcTableCell_ptr Cell = pfcTableCell::Create(trcont, tccont);
        //增加部分添加新行新列，行高1.5字符，列宽根据内容确定
        if (rcont > trcont)
        {
            for (int i = 0; i < (rcont - trcont); i++)
            {
                Table->InsertRow(1.5, trcont + i, true);
            }
        }
        if (ccont > tccont)
        {
            for (int j = 0; j < (ccont - tccont); j++)
            {
                int cwidth = 1;
                for (int r = 0; r < rcont; r++)
                {
                    BE::BasicExcelCell *wc = ws->Cell(r, tccont + j);
                    int tmpwidth = wc->GetStringLength();
                    if (tmpwidth > cwidth)
                        cwidth = tmpwidth;
                }
                Table->InsertColumn(cwidth, tccont + j, true);
            }
        }
        for (int r = 0; r < rcont; r++)
        {
            for (int c = 0; c < ccont; c++)
            {
                pfcTableCell_ptr TableCell = pfcTableCell::Create(r + 1, c + 1);
                xstringsequence_ptr CellTexts = GenCellTexts(ws, r, c);
                if (CellTexts->getarraysize() > 0)
                {
                    Table->SetText(TableCell, CellTexts);
                }
            }
        }
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (exception e)
    {
        fprintf(exception_info, "Err:%s\n", e.what());
    }
    Drawing->Regenerate();
    fclose(exception_info);
    return wfcTK_NO_ERROR;
}
//创建表格
wfcStatus CreateTable()
{
    OPENEXCEPTIONFILE("a");
    pfcSession_ptr Session = pfcGetProESession();
    pfcModel_ptr Model = Session->GetCurrentModel();
    pfcDrawing_ptr Drawing = pfcDrawing::cast(Model);
    pfcFileOpenOptions_ptr FileOpenOpts = pfcFileOpenOptions::Create("*.xls,*.xlsx");
    FileOpenOpts->SetDefaultPath(DllPath.c_str());
    xstring filepath = Session->UIOpenFile(FileOpenOpts);
    BE::BasicExcel wb;
    if (wb.Load(filepath.operator cStringT()) == false)
    {
        Session->UIDisplayMessage(MSGFILE, "File is opened!", NULL);
        return wfcTK_NO_ERROR;
    }
    BE::BasicExcelWorksheet *ws = wb.GetWorksheet(0);
    int rcont, ccont;
    rcont = ws->GetTotalRows();
    ccont = ws->GetTotalCols();
    Session->UIDisplayMessage(MSGFILE, "Click Mouse Left Button...", NULL);
    pfcPoint3D_ptr TableLoc = Session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
    Session->UIClearMessage();
    pfcTableCreateInstructions_ptr TableCreateInst = pfcTableCreateInstructions::Create(TableLoc);
    TableCreateInst->SetSizeType(pfcTABLESIZE_BY_NUM_CHARS);
    pfcColumnCreateOptions_ptr CCOpts = pfcColumnCreateOptions::createCapacity(ccont);
    for (int c = 0; c < ccont; c++)
    {
        int cwidth = 1;
        for (int r = 0; r < rcont; r++)
        {
            BE::BasicExcelCell *wc = ws->Cell(r, c);
            int tmpwidth = wc->GetStringLength(); //获取内容字符数量，不区分中英文
            if (tmpwidth > cwidth)
                cwidth = tmpwidth;
        }
        //单元格水平居中 Creo中中英文字符宽度相同 列宽直接用字符数+1
        pfcColumnCreateOption_ptr CCopt = pfcColumnCreateOption::Create(pfcCOL_JUSTIFY_CENTER, cwidth + 1);
        CCOpts->set(c, CCopt);
    }
    TableCreateInst->SetColumnData(CCOpts);
    xrealsequence_ptr RHeights = xrealsequence::createCapacity(rcont);
    for (int r = 0; r < rcont; r++)
    {
        xreal RHeight = 1.5; //行高1.5字符
        RHeights->set(r, RHeight);
    }
    TableCreateInst->SetRowHeights(RHeights);
    pfcTable_ptr Table = Drawing->CreateTable(TableCreateInst);
    for (int r = 0; r < rcont; r++)
    {
        for (int c = 0; c < ccont; c++)
        {
            pfcTableCell_ptr TableCell = pfcTableCell::Create(r + 1, c + 1);
            xstringsequence_ptr CellTexts = GenCellTexts(ws, r, c);
            if (CellTexts->getarraysize() > 0)
            {
                Table->SetText(TableCell, CellTexts);
            }
        }
    }
    Table->Display();
    Drawing->Regenerate();
    fclose(exception_info);
    return wfcTK_NO_ERROR;
}
//计算表格的四个顶点
pfcPoint3Ds_ptr GetCorner(pfcOutline3D_ptr TableOutline)
{
    pfcPoint3Ds_ptr points = pfcPoint3Ds::create();
    double value[4];
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            value[i * 2 + j] = TableOutline->get(i)->get(j);
        }
    }
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            pfcPoint3D_ptr tpoint = pfcPoint3D::create();
            tpoint->set(0, value[i * 2]);
            tpoint->set(1, value[j * 2 + 1]);
            points->append(tpoint);
        }
    }
    return points;
}
//从四个顶点中找出距离当前鼠标位置最近的点
pfcPoint3D_ptr GetClosest(pfcPoint3Ds_ptr points, pfcPoint3D_ptr MousePoint)
{
    double distance = DBL_MAX;
    int index = 0;
    for (int i = 0; i < 4; i++)
    {
        double td = Cal_Distance(MousePoint, points->get(i));
        if (td < distance)
        {
            distance = td;
            index = i;
        }
    }
    return points->get(index);
}
//获得表格角点坐标
wfcStatus GetCornerCoor()
{
    OPENEXCEPTIONFILE("a");
    if (CheckEnv(pfcMDL_DRAWING) == 0)
    {
        fclose(exception_info);
        return wfcTK_NO_ERROR;
    }
    pfcSession_ptr session = pfcGetProESession();
    pfcModel_ptr model = session->GetCurrentModel();
    try
    {
        if (model->GetType() == pfcMDL_DRAWING)
        {
            pfcDrawing_ptr drawing = pfcDrawing::cast(model);
            xint SheetNumber = drawing->GetCurrentSheetNumber();
            pfcTransform3D_ptr SheetTrans = drawing->GetSheetTransform(SheetNumber);
            pfcSelectionOptions_ptr SelOpts = pfcSelectionOptions::Create("dwg_table");
            SelOpts->SetMaxNumSels(1);
            pfcTables_ptr tables = pfcTables::create();
            while (true)
            {
                pfcSelections_ptr sels = session->Select(SelOpts, NULL);
                if (sels != NULL)
                {
                    pfcModelItem_ptr ModelItem = sels->get(0)->GetSelItem();
                    pfcTable_ptr Table;
                    if (ModelItem->GetType() == pfcITEM_TABLE)
                    {
                        Table = pfcTable::cast(ModelItem);
                        tables->append(Table);
                        pfcTableInfo_ptr TableInfo = Table->GetInfo(0);
                        pfcOutline3D_ptr TableOutline = TableInfo->GetOutline();
                        for (int i = 0; i < 2; i++)
                        {
                            TableOutline->set(i, SheetTrans->TransformPoint(TableOutline->get(i)));
                        }
                        pfcPoint3Ds_ptr points = GetCorner(TableOutline);
                        pfcPoint3D_ptr MousePosition = session->UIGetCurrentMouseStatus(false)->GetPosition();
                        MousePosition = SheetTrans->TransformPoint(MousePosition);
                        pfcPoint3D_ptr closest = GetClosest(points, MousePosition);
                        string linecoor;
                        xstringsequence_ptr seqcoor = xstringsequence::create();
                        linecoor += "x=";
                        linecoor += to_string(closest->get(0));
                        linecoor += ",y=";
                        linecoor += to_string(closest->get(1));
                        linecoor += ",";
                        seqcoor->append(linecoor.c_str());
                        session->UIDisplayMessage(MSGFILE, "Corner Point:%0s", seqcoor);
                        drawing->Regenerate();
                    }
                    else
                        continue;
                }
                else
                {
                    break;
                }
            }
        }
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (const std::exception &e)
    {
        fprintf(exception_info, "Err:%s\n", e.what());
    }
    fclose(exception_info);
    return wfcTK_NO_ERROR;
}