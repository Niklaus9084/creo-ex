#include <OTKXMain.h>

#include <ProMdl.h>
#include <ProWstring.h>
#include <ProDrawing.h>
#include <ProWindows.h>

typedef enum _exporttype
{
	Export_Drw,
	Export_Pdf,
	Export_Stp,
} ExportType;
//检索同名工程图
int _RetrieveDrawing(pfcModel_ptr Model, pfcModel_ptr *Drawing)
{
	OPENEXCEPTIONFILE("a");
	pfcSession_ptr Session = pfcGetProESession();
	try
	{
		string FullName = Model->GetFullName();
		pfcModelDescriptor_ptr ModelDesc = Model->GetDescr();
		pfcModelDescriptor_ptr DrawingDescr = pfcModelDescriptor::CreateFromFileName((FullName + ".drw").c_str());
		DrawingDescr->SetPath(Session->GetCurrentDirectory());
		pfcRetrieveModelOptions_ptr DrawingRet = pfcRetrieveModelOptions::Create();
		DrawingRet->SetAskUserAboutReps(false);
		*Drawing = Session->RetrieveModelWithOpts(DrawingDescr, DrawingRet);
		fclose(exception_info);
		return 1;
	}
	xcatchbegin
		xcatch(pfcXToolkitError, pfcex) {}
	xcatchend catch (exception e)
	{
		fprintf(exception_info, "Msg:%s\n", e.what());
	}
	fclose(exception_info);
	return 0;
}
//目前只能打开工作目录下和工作区内图纸
//是否增加打开模型所在目录图纸待考虑
wfcStatus _OpenDrawing(pfcModel_ptr Model)
{
	OPENEXCEPTIONFILE("a");
	pfcSession_ptr Session = pfcGetProESession();
	string FullName = Model->GetFullName();

	pfcModel_ptr Drawing;
	//未检索到模型输出错误信息
	if (!_RetrieveDrawing(Model, &Drawing))
	{
		xstringsequence_ptr stringseq = xstringsequence::create();
		stringseq->append(FullName.c_str());
		Session->UIDisplayMessage(MSGFILE, "%0s Not Find Drawing!", stringseq);
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	else
	{
		Drawing->DisplayInNewWindow();
		pfcWindow_ptr Window = Session->GetModelWindow(Drawing);
		Session->SetCurrentWindow(Window);
		Window->Activate();
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
}
wfcStatus OpenDrawing()
{
	if (CheckEnv(pfcMDL_PART, pfcMDL_ASSEMBLY) == 0)
	{
		return wfcTK_NO_ERROR;
	}
	OPENEXCEPTIONFILE("a");
	pfcSession_ptr Session = pfcGetProESession();
	pfcModel_ptr Model;
	try
	{
		pfcSelectionBuffer_ptr SelectionBuffer = Session->GetCurrentSelectionBuffer();
		pfcSelections_ptr Sels = SelectionBuffer->GetContents();
		pfcSelectionOptions_ptr SelectionOptions = pfcSelectionOptions::Create("prt_or_asm");
		SelectionOptions->SetMaxNumSels(-1);
		pfcSelections_ptr Selections = Session->Select(SelectionOptions, Sels);
		pfcWindow_ptr Window = Session->GetCurrentWindow();
		Window->Repaint();
		//未选择模型则打开当前模型工程图
		if (Selections == NULL)
		{
			Model = Session->GetCurrentModel();
			_OpenDrawing(Model);
		}

		else
		{
			xint ModelsNum = Selections->getarraysize();
			for (xint i = 0; i < ModelsNum; i++)
			{
				Model = Selections->get(i)->GetSelModel();
				_OpenDrawing(Model);
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Msg:%s\n", e.what());
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
void _MoveDrawing(pfcModels_ptr &Models)
{
	OPENEXCEPTIONFILE("a");
	pfcSession_ptr Session = pfcGetProESession();
	pfcDirectorySelectionOptions_ptr Options = pfcDirectorySelectionOptions::Create();
	xstring AimPath = Session->UISelectDirectory(Options);
	try
	{
		for (int i = 0; i < Models->getarraysize(); i++)
		{
			pfcModel_ptr Model = Models->get(i);
			pfcModel_ptr Drawing;
			if (_RetrieveDrawing(Model, &Drawing))
			{
				pfcModelDescriptor_ptr ModelDescr = Drawing->GetDescr();
				ModelDescr->SetPath(AimPath);
				Drawing->Backup(ModelDescr);
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Msg:%s\n", e.what());
	}
	fclose(exception_info);
}
//手动设置pdf导出配置 暂时无用
pfcPDFOptions_ptr _GetPdfOptions()
{
	OPENEXCEPTIONFILE("a");
	pfcPDFOptions_ptr PDFOptions = pfcPDFOptions::create();
	string line;
	int pos;
	pfcPDFOption_ptr PDFOption1 = pfcPDFOption::Create();
	PDFOption1->SetOptionType(pfcPDFOPT_FONT_STROKE);
	xint IntArgValue1 = pfcPDF_STROKE_ALL_FONTS;
	pfcArgValue_ptr OptionValue1 = pfcCreateIntArgValue(IntArgValue1);
	PDFOption1->SetOptionValue(OptionValue1);
	PDFOptions->append(PDFOption1);
	pfcPDFOption_ptr PDFOption2 = pfcPDFOption::Create();
	PDFOption2->SetOptionType(pfcPDFOPT_LAUNCH_VIEWER);
	pfcArgValue_ptr OptionValue2 = pfcCreateBoolArgValue(false);
	PDFOption2->SetOptionValue(OptionValue2);
	PDFOptions->append(PDFOption2);

	fclose(exception_info);
	return PDFOptions;
}
void _Drw2Pdf(pfcModels_ptr &Models)
{
	OPENEXCEPTIONFILE("a");
	//pdf导出配置文件
	string profilepath = DllPath + "export_data\\Drawing_Batch_Export.dop";
	pfcSession_ptr Session = pfcGetProESession();
	pfcDirectorySelectionOptions_ptr Options = pfcDirectorySelectionOptions::Create();
	xstring AimPath = Session->UISelectDirectory(Options);
	pfcWindow_ptr CurrentWindow = Session->GetCurrentWindow();
	try
	{
		for (int i = 0; i < Models->getarraysize(); i++)
		{
			pfcModel_ptr Model = Models->get(i);
			pfcModel_ptr Drawing;
			if (_RetrieveDrawing(Model, &Drawing))
			{

				Drawing->DisplayInNewWindow();
				pfcWindow_ptr Window = Session->GetModelWindow(Drawing);
				Session->SetCurrentWindow(Window);
				Window->Activate();
				Window->Refresh();
				xstring FullName, Filename;
				FullName = Model->GetFullName();
				Filename = FullName + ".pdf";
				pfcPDFExportInstructions_ptr ExportInst = pfcPDFExportInstructions::Create();
				ExportInst->SetFilePath(NULL);
				ExportInst->SetProfilePath(profilepath.c_str());
				Drawing->Export(AimPath + "\\" + Filename, (pfcExportInstructions_ptr)ExportInst);
				Window->Close();
				CurrentWindow->Activate();
			}
		}
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Msg:%s\n", e.what());
	}
	fclose(exception_info);
}
void _3D2Stp(pfcModels_ptr &Models)
{
	OPENEXCEPTIONFILE("a");
	pfcSession_ptr Session = pfcGetProESession();
	pfcDirectorySelectionOptions_ptr Options = pfcDirectorySelectionOptions::Create();
	xstring AimPath = Session->UISelectDirectory(Options);
	pfcWindow_ptr CurrentWindow = Session->GetCurrentWindow();
	try
	{
		pfcGeometryFlags_ptr GeomFlags = pfcGeometryFlags::Create();
		GeomFlags->SetAsSolids(true);
		GeomFlags->SetAsSurfaces(false);
		GeomFlags->SetAsQuilts(false);
		GeomFlags->SetAsWireframe(false);
		pfcSTEP3DExportInstructions_ptr Step3DExportInst;
		Step3DExportInst = pfcSTEP3DExportInstructions::Create(pfcEXPORT_ASM_SINGLE_FILE, GeomFlags);
		pfcInclusionFlags_ptr InclusionFlags = pfcInclusionFlags::Create();
		InclusionFlags->SetIncludeFaceted(false);
		Step3DExportInst->SetIncludedEntities(InclusionFlags);
		for (int i = 0; i < Models->getarraysize(); i++)
		{
			pfcModel_ptr Model = Models->get(i);
			Model->DisplayInNewWindow();
			pfcWindow_ptr Window = Session->GetModelWindow(Model);
			Session->SetCurrentWindow(Window);
			Window->Activate();
			Window->Refresh();
			xstring FullName, Filename, profilepath;
			FullName = Model->GetFullName();
			Filename = FullName + ".stp";
			Model->Export(AimPath + "\\" + Filename, (pfcExportInstructions_ptr)Step3DExportInst);
			if (i)
				Window->Close();
		}
		CurrentWindow->Activate();
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (exception e)
	{
		fprintf(exception_info, "Msg:%s\n", e.what());
	}
	fclose(exception_info);
}

wfcStatus Export(ExportType exporttype)
{
	{
		if (CheckEnv(pfcMDL_PART, pfcMDL_ASSEMBLY) == 0)
		{
			return wfcTK_NO_ERROR;
		}
		OPENEXCEPTIONFILE("a");
		pfcSession_ptr Session = pfcGetProESession();
		pfcModel_ptr Model;
		try
		{
			Model = Session->GetCurrentModel();
			pfcModelType ModelType = Model->GetType();
			pfcModels_ptr Models = pfcModels::create();
			if (ModelType == pfcMDL_PART)
				Models->append(Model);
			else if (ModelType == pfcMDL_ASSEMBLY)
			{
				Models->append(Model);
				_Traversal_ModelTree(Model, Models);
			}
			if (Models->getarraysize() > 0)
			{
				if (exporttype == Export_Pdf)
					_Drw2Pdf(Models);
				else if (exporttype == Export_Stp)
					_3D2Stp(Models);
				else if (exporttype == Export_Drw)
					_MoveDrawing(Models);
			}
		}
		OTK_EXCEPTION_HANDLER(exception_info)
		catch (exception e)
		{
			fprintf(exception_info, "Msg:%s\n", e.what());
		}
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
}

wfcStatus Drw2Pdf()
{
	return Export(Export_Pdf);
}
wfcStatus MoveDrawing()
{
	return Export(Export_Drw);
}
wfcStatus Export2Stp()
{
	return Export(Export_Stp);
}
