#include <OTKXMain.h>

//创建固定约束
pfcComponentConstraints_ptr CreateConstraints()
{
    pfcComponentConstraint_ptr Constraint;
    Constraint = pfcComponentConstraint::Create(pfcASM_CONSTRAINT_FIX);
    pfcComponentConstraints_ptr ret;
    ret = pfcComponentConstraints::create();
    ret->append(Constraint);
    return ret;
}
//模型约束全部固定
wfcStatus FixAllComponent()
{
    pfcSession_ptr session = pfcGetProESession();
    if (CheckEnv(pfcMDL_ASSEMBLY) == 0)
    {
        return wfcTK_NO_ERROR;
    }
    OPENEXCEPTIONFILE("a");
    try
    {
        pfcModel_ptr model = session->GetCurrentModel();
        pfcFeatures_ptr features = pfcFeatures::create();
        _Traversal_ComponentTree(model, features);
        pfcComponentConstraints_ptr FixConstraints = CreateConstraints();
        pfcComponentConstraints_ptr AutoConstraints = pfcComponentConstraints::create();
        //去掉运动约束 改为用户定义
        for (int i = 0; i < features->getarraysize(); i++)
        {
            pfcComponentFeat_ptr feature = pfcComponentFeat::cast(features->get(i));
            //骨架模型没有约束，未找到骨架模型识别方式，利用try-catch跳过，同时可跳过其它无法设置约束类型
            try
            {
                feature->SetConstraints(AutoConstraints, NULL);
                feature->SetCompType(pfcCOMPONENT_NO_DEF_ASSUM);
                feature->Regenerate();
            }
            OTK_EXCEPTION_HANDLER(exception_info)
        }
        //改为固定约束
        for (int i = 0; i < features->getarraysize(); i++)
        {
            pfcComponentFeat_ptr feature = pfcComponentFeat::cast(features->get(i));
            try
            {
                feature->SetConstraints(FixConstraints, NULL);
            }
            OTK_EXCEPTION_HANDLER(exception_info)
        }
    }
    OTK_EXCEPTION_HANDLER(exception_info)
    catch (exception e)
    {
        fprintf(exception_info, "Msg:%s\n", e.what());
    }
    fclose(exception_info);
    return wfcTK_NO_ERROR;
}