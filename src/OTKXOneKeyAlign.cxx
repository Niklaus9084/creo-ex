#include <OTKXMain.h>

int dimdistance = 24;

int CheckEnv();

wfcStatus Align(Alignment alignment)
{
	OPENEXCEPTIONFILE("a");
	try
	{
		if (CheckEnv() == 0)
		{
			return wfcTK_NO_ERROR;
		}
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcDrawing_ptr drawing = pfcDrawing::cast(mdl);
		pfcSelectionBuffer_ptr SelectionBuffer = session->GetCurrentSelectionBuffer();
		pfcSelections_ptr Selections = SelectionBuffer->GetContents();
		if (Selections == NULL)
			return wfcTK_NO_ERROR;
		pfcPoint3D_ptr dimloc;
		pfcPoint3D_ptr mousepos = session->UIGetNextMousePick(pfcMOUSE_BTN_LEFT)->GetPosition();
		if (alignment != -1)
		{
			xint flag = !alignment;
			for (int i = 0; i < Selections->getarraysize(); i++)
			{
				pfcModelItem_ptr ModelItem = Selections->get(i)->GetSelItem();
				//判断类型是否为尺寸或参考尺寸
				if (ModelItem->GetType() == pfcITEM_DIMENSION || ModelItem->GetType() == pfcITEM_REF_DIMENSION)
				{
					pfcDimension2D_ptr dimension = pfcDimension2D::cast(ModelItem);
					dimloc = dimension->GetLocation();
					dimloc->set(flag, mousepos->get(flag));
					ChangeDimensionLocation(dimension, dimloc);
					pfcPoint3Ds_ptr Points = pfcPoint3Ds::create();
					SetDimensionMid(dimension, Points);
				}
				//判断类型为球标 非与重复区域关联的球标
				else if (ModelItem->GetType() == pfcITEM_DTL_NOTE)
				{
					pfcDetailItem_ptr DetailItem = pfcDetailItem::cast(ModelItem);
					if (DetailItem->GetDetailType() == pfcDETAIL_NOTE)
					{
						pfcDetailNoteItem_ptr NoteItem = pfcDetailNoteItem::cast(DetailItem);
						pfcDetailNoteInstructions_ptr NoteInst = NoteItem->GetInstructions(false);
						pfcDetailLeaders_ptr NoteLeaders = NoteInst->GetLeader();
						pfcAttachment_ptr NoteAttachment = NoteLeaders->GetItemAttachment();
						if (NoteAttachment->GetType() == pfcATTACH_FREE)
						{
							pfcFreeAttachment_ptr NoteFreeAttachment = pfcFreeAttachment::cast(NoteAttachment);
							pfcPoint3D_ptr NoteLoc = NoteFreeAttachment->GetAttachmentPoint();
							NoteLoc->set(flag, mousepos->get(flag));
							pfcFreeAttachment_ptr NoteFreeAttachmentNew = pfcFreeAttachment::Create(NoteLoc);
							NoteLeaders->SetItemAttachment((pfcAttachment_ptr)NoteFreeAttachmentNew);
							NoteInst->SetLeader(NoteLeaders);
							pfcAttachments_ptr NoteAttachments = NoteLeaders->GetLeaders();
							bool IsModified = true;
							if (NoteAttachments->getarraysize() > 0)
							{
								for (xint i = 0; i < NoteAttachments->getarraysize(); i++)
								{
									//与重复区域关联的球标无法使用pfcDetailNoteItem_ptr->Modify更改位置
									if (NoteAttachments->get(i)->GetType() == pfcATTACH_PARAMETRIC)
										IsModified = false;
								}
							}
							if (IsModified)
								NoteItem->Modify(NoteInst);
						}
					}
				}
				else
				{
					continue;
					fprintf(exception_info, "DetailItem Type:%d\n", ModelItem->GetType());
				}
			}
		}
		else
		{
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		drawing->Regenerate();
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Align Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}

wfcStatus Distribution(Alignment alignment)
{
	OPENEXCEPTIONFILE("a");
	try
	{
		pfcSession_ptr session = pfcGetProESession();
		wfcWSession_ptr wsession = wfcWSession::cast(pfcGetProESession());
		if (CheckEnv() == 0)
		{
			fclose(exception_info);
			return wfcTK_NO_ERROR;
		}
		pfcModel_ptr mdl = session->GetCurrentModel();
		pfcDrawing_ptr drawing = pfcDrawing::cast(mdl);
		pfcSelectionBuffer_ptr SelectionBuffer = session->GetCurrentSelectionBuffer();
		pfcSelections_ptr Selections = SelectionBuffer->GetContents();
		if (Selections == NULL)
			return wfcTK_NO_ERROR;
		int numitems = Selections->getarraysize();
		pfcDimension2Ds_ptr dimensions = pfcDimension2Ds::create();
		int numdimensions;
		// 收集尺寸或参考尺寸
		for (int i = 0; i < numitems; i++)
		{
			pfcModelItem_ptr ModelItem = Selections->get(i)->GetSelItem();
			xstring tmpstring = Selections->get(i)->GetSelectionString();
			if (tmpstring.IsEmpty())
				continue;
			if (ModelItem->GetType() == pfcITEM_DIMENSION || ModelItem->GetType() == pfcITEM_REF_DIMENSION)
			{
				dimensions->append(pfcDimension2D::cast(ModelItem));
			}
		}

		if (alignment != -1)
		{
			xint flag = alignment;
			numdimensions = dimensions->getarraysize();
			//尺寸排序
			for (int i = 0; i < numdimensions - 1; i++)
			{
				for (int j = 0; j < numdimensions - i - 1; j++)
				{
					pfcPoint3D_ptr location1 = dimensions->get(j)->GetLocation();
					pfcPoint3D_ptr location2 = dimensions->get(j + 1)->GetLocation();
					pfcDimension2D_ptr tempdim;
					if (location1->get(flag) > location2->get(flag))
					{
						tempdim = dimensions->get(j);
						dimensions->set(j, dimensions->get(j + 1));
						dimensions->set(j + 1, tempdim);
					}
				}
			}

			xint baseloc = dimensions->get(0)->GetLocation()->get(flag);
			//修改尺寸位置
			for (int i = 1; i < numdimensions; i++)
			{
				pfcDimension2D_ptr CurrentDim = dimensions->get(i);
				pfcPoint3D_ptr location2 = CurrentDim->GetLocation();
				location2->set(flag, baseloc + dimdistance * i);
				ChangeDimensionLocation(CurrentDim, location2);
				pfcPoint3Ds_ptr points = pfcPoint3Ds::create();
				SetDimensionMid(CurrentDim, points);
			}
		}
		drawing->Regenerate();
		fclose(exception_info);
		return wfcTK_NO_ERROR;
	}
	OTK_EXCEPTION_HANDLER(exception_info)
	catch (...)
	{
		fputs("Distribution Failed!\n", exception_info);
	}
	fclose(exception_info);
	return wfcTK_NO_ERROR;
}
wfcStatus VerticalAlign()
{
	return Align(Vertical);
}
wfcStatus HorizonAlign()
{
	return Align(Horizon);
}
wfcStatus VDistribution()
{
	return Distribution(Vertical);
}
wfcStatus HDistribution()
{
	return Distribution(Horizon);
}
wfcStatus SetDistance()
{
	pfcSession_ptr Session = pfcGetProESession();
	Session->UIDisplayLocalizedMessage(MSGFILE, "Please Input distribution distance:", NULL);
	dimdistance = Session->UIReadIntMessage(0, 100);
	return wfcTK_NO_ERROR;
}