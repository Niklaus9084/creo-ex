#ifndef otkxmath_h_
#define otkxmath_h_
//VS2012中没有round函数
double round(double r)
{
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}
const float PI = 3.14159265358979323846;
// NaN = 0.0/0.0, 0.0*inf, inf/inf
bool IsNumber(double x)
{
    //对于IEEE 754浮点数NaN来说总会得到false
    return (x == x);
}
// inf = 1/0
bool IsFiniteNumber(double x)
{
    return (x <= DBL_MAX && x >= -DBL_MAX);
}
double Cal_Distance(pfcPoint3D_ptr point1, pfcPoint3D_ptr point2)
{
    double d[3];
    for (int i = 0; i < 3; i++)
    {
        d[i] = point1->get(i) - point2->get(i);
    }
    double distance2 = 0;
    for (int i = 0; i < 3; i++)
    {
        distance2 += d[i] * d[i];
    }
    return pow(distance2, 0.5);
}
#endif