#ifndef OTKXLINK_H_
#define OTKXLINK_H_
#include <OTKXMain.h>

template <class T>
struct LinkNode
{
public:
    T element;
    LinkNode *next;
    LinkNode *last;

public:
    LinkNode(){};
    LinkNode(T e, LinkNode *last, LinkNode *next)
    {
        this->element = e;
        this->next = next;
        this->last = last;
    }
};
template <class T>
class LinkList
{
public:
    LinkList();
    ~LinkList();

    int size();
    bool is_empty();

    T get(int index);
    T get_head();
    T get_tail();

    int insert(int index, T t);
    int insert_head(T t);
    int append_tail(T t);

    int del(int index);
    int delete_head();
    int delete_tail();

    void swap(int index1, int index2);
    //void print();

private:
    int count;
    LinkNode<T> *head;

private:
    LinkNode<T> *get_node(int index);
};
//模板类的定义与实现必须放在一个文件下
template class LinkList<DimensionStruct>;
class DimensionList : public LinkList<DimensionStruct>
{
};
template class LinkList<SymbolStruct>;
class SymbolList : public LinkList<SymbolStruct>
{
};
//构造函数
template <class T>
LinkList<T>::LinkList() : count(0)
{
    //创建表头 没有数据
    head = new LinkNode<T>();
    head->last = head->next = head;
}
//析构函数
template <class T>
LinkList<T>::~LinkList()
{
    LinkNode<T> *tmp;
    LinkNode<T> *node = head->next;
    while (node != head)
    {
        tmp = node;
        node = node->next;
        delete tmp;
    }
    delete head;
    head = NULL;
}
//返回节点数
template <class T>
int LinkList<T>::size()
{
    return count;
}
//判断链表是否为空
template <class T>
bool LinkList<T>::is_empty()
{
    return count == 0;
}
//获取index位置节点数据
template <class T>
LinkNode<T> *LinkList<T>::get_node(int index)
{
    //判断参数有效性
    if (index < 0 || index >= count)
    {
        return NULL;
    }
    //正向查找
    if (index <= count / 2)
    {
        int i = 0;
        LinkNode<T> *plindex = head->next;
        while (i++ < index)
        {
            plindex = plindex->next;
        }
        return plindex;
    }
    //反向查找
    int j = 0;
    int rindex = count - index - 1;
    LinkNode<T> *prindex = head->last;
    while (j++ < rindex)
    {
        prindex = prindex->last;
    }
    return prindex;
}
template <class T>
T LinkList<T>::get(int index)
{
    return get_node(index)->element;
}
template <class T>
T LinkList<T>::get_head()
{
    return get_node(0)->element;
}
template <class T>
T LinkList<T>::get_tail()
{
    return get_node(count - 1)->element;
}
//将节点插入第index位置之前
template <class T>
int LinkList<T>::insert(int index, T t)
{
    if (index == 0)
        return insert_head(t);
    LinkNode<T> *pindex = get_node(index);
    LinkNode<T> *pnode = new LinkNode<T>(t, pindex->last, pindex);
    pindex->last->next = pnode;
    pindex->last = pnode;
    count++;
    return 0;
}
//将节点插入第1个节点处
template <class T>
int LinkList<T>::insert_head(T t)
{
    LinkNode<T> *pnode = new LinkNode<T>(t, head, head->next);
    head->next->last = pnode;
    head->next = pnode;
    count++;
    return 0;
}
//将节点追加到链表末尾
template <class T>
int LinkList<T>::append_tail(T t)
{
    LinkNode<T> *pnode = new LinkNode<T>(t, head->last, head);
    head->last->next = pnode;
    head->last = pnode;
    count++;
    return 0;
}
//删除index位置节点
template <class T>
int LinkList<T>::del(int index)
{
    LinkNode<T> *pindex = get_node(index);
    pindex->next->last = pindex->last;
    pindex->last->next = pindex->next;
    delete pindex;
    count--;
    return 0;
}
//删除第一节点
template <class T>
int LinkList<T>::delete_head()
{
    return del(0);
}
//删除最后一个节点
template <class T>
int LinkList<T>::delete_tail()
{
    return del(count - 1);
}
//交换两个节点
template <class T>
void LinkList<T>::swap(int index1, int index2)
{
    LinkNode<T> *pindex1 = get_node(index1);
    LinkNode<T> *pindex2 = get_node(index2);
    pindex1->last->next = pindex2;
    pindex2->last = pindex1->last;
    pindex1->next = pindex2->next;
    pindex2->next->last = pindex1;
    pindex2->next = pindex1;
    pindex1->last = pindex2;
}
#endif
