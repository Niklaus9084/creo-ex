
#include <wfcClient.h>
#include <wfcSession.h>
#include <wfcGlobal.h>
#include <pfcSession.h>
#include <pfcCommand.h>
#include <pfcSelect.h>
#include <ProSelbuffer.h>
#include <pfcSolid.h>
#include <pfcDrawing.h>
#include <pfcTable.h>
#include <wfcTable.h>
#include <pfcUI.h>
#include <pfcNote.h>
#include <pfcExceptions.h>
#include <OTKXUtils.h>
#include <OTKXLink.h>
#include <OTKXDimension.h>
#include <OTKXmath.h>
#include <OTKXSketch.h>
#include <OTKXString.h>
#include <OTKXAssembly.h>
#include <OTKXListeners.h>

#include <ExcelFormat.h>
#include <Windows.h>
#include <map>
#include <math.h>
#include <time.h>
#include <regex>
#include <string>
#include <iostream>
#include <memory> //unique_ptr 智能指针
#include <stdio.h>
#include <atlstr.h> //CString所在头文件

#ifdef GetMessage
#undef GetMessage
#endif

#ifdef UNICODE
#undef UNICODE
#endif

#ifdef GetCurrentDirectory
#undef GetCurrentDirectory
#endif

#ifdef exception_info
#undef exception_info
#endif

;
void CreateLine(pfcDrawing_ptr drawing,
                pfcView2D_ptr View2D,
                pfcPoint3D_ptr begin,
                pfcPoint3D_ptr end,
                pfcDetailItem_ptr *DetailItem);
int CheckEnv();
int CheckEnv(pfcModelType ModelType);
int CheckEnv(pfcModelType ModelType1, pfcModelType ModelType2);

namespace BE = ExcelFormat;
#define MSGFILE "msg.txt"
#define EXCEPTIONFILENAME "D:\\PTC\\exception_info.inf"
#define OPENEXCEPTIONFILE(flag) FILE *exception_info = fopen(EXCEPTIONFILENAME, flag)
#define eps 1e-4

#ifndef otkxmain_h_
#define otkxmain_h_

string DllPath;

#endif