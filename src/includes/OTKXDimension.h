#ifndef otkxdimension_h_
#define otkxdimension_h_
#include <OTKXMain.h>
#include <OTKXLink.h>
typedef enum _direction
{
    Vertical = 1,
    Horizon = 0,
    Direction_nil = -1
} Alignment;
int pad = 10;
void ChangeDimensionLocation(pfcDimension2D_ptr dimension, pfcPoint3D_ptr dimloc);
int CalDimensionValueWideth(pfcDimension2D_ptr dimension);
void SetDimensionMid(pfcDimension2D_ptr dimension, pfcPoint3Ds_ptr &Points);
class DimensionStruct
{
private:
    pfcDimension2D_ptr dimension;
    pfcPoint3D_ptr location;
    double angle;
    double distance;
    pfcPoint3D_ptr viewcenter;
    xstring viewname;
    pfcView2D_ptr view2d;

public:
    void Create(pfcDimension2D_ptr dim);
    pfcDimension2D_ptr GetDimension();
    pfcPoint3D_ptr GetViewCenter();
    double GetAngle();
    double GetDistance();
};
class SymbolStruct
{
public:
    pfcDetailSymbolInstItem_ptr SymbolInst;
    int index;
};

#endif
