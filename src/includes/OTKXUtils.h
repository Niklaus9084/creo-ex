/*
	Copyright (c) 2017 PTC Inc. and/or Its Subsidiary Companies. All Rights Reserved.
*/

#ifndef OTKXUTILS_H
#define OTKXUTILS_H

#include <fstream>
#include <pfcExceptions.h>
#include <pfcLayer.h>

/* Writes exception(first parameter in macro) into file (second parameter in macro) */
#define OTK_EXCEPTION_WRITE(pfcex, fp_out)                                                  \
    {                                                                                       \
        xstring msg = pfcex->GetMessage();                                                  \
        xstring method = pfcex->GetMethodName();                                            \
        xint err = pfcex->GetErrorCode();                                                   \
        fprintf(fp_out, "pfcXToolkitError in method <%s> with error <%d>\n message <%s>\n", \
                ((string)method).c_str(), err, ((string)msg).c_str());                      \
        fclose(fp_out);                                                                     \
    }

/* Prints exception on terminal */
#define OTK_EXCEPTION_PRINT(pfcex)                                                                               \
    {                                                                                                            \
        xstring msg = pfcex->GetMessage();                                                                       \
        xstring method = pfcex->GetMethodName();                                                                 \
        xint err = pfcex->GetErrorCode();                                                                        \
        cout << "pfcXToolkitError in method " << method << " with error " << err << "\n message " << msg << "\n" \
             << endl;                                                                                            \
    }

/* Writes uncaught/default exception into file (second parameter in macro) */
#define OTK_DEFAULT_EXCEPTION_WRITE(defaultex, fp_out)                     \
    {                                                                      \
        xstring msg = defaultex->getCipTypeName();                         \
        fprintf(fp_out, "Uncaught %s exception\n", ((string)msg).c_str()); \
        fclose(fp_out);                                                    \
    }

/* Prints exception on terminal */
#define OTK_DEFAULT_EXCEPTION_PRINT(ftypeex)          \
    {                                                 \
        xstring msg = ftypeex->getCipTypeName();      \
        cout << "Uncaught " << msg << " exception \n" \
             << endl;                                 \
    }

/* Writes invalid file info into file (second parameter in macro) */
#define OTK_FILENAME_EXCEPTION_WRITE(fname, fp_out)                        \
    {                                                                      \
        xstring msg = fname->GetMessage();                                 \
        fprintf(fp_out, "Invalid File type: %s\n", ((string)msg).c_str()); \
        fclose(fp_out);                                                    \
    }

/* Prints invalid file info on terminal */
#define OTK_FILENAME_EXCEPTION_PRINT(fname)          \
    {                                                \
        xstring msg = fname->GetMessage();           \
        cout << "Invalid File type: " << msg << "\n" \
             << endl;                                \
    }

/* Writes invalid mdl extension info into file (second parameter in macro) */
#define OTK_MDLEXT_EXCEPTION_WRITE(mdlext, fp_out)                         \
    {                                                                      \
        xstring msg = mdlext->GetMessage();                                \
        fprintf(fp_out, "Invalid File type: %s\n", ((string)msg).c_str()); \
        fclose(fp_out);                                                    \
    }

/* Prints invalid mdl extension info on terminal */
#define OTK_MDLEXT_EXCEPTION_PRINT(mdlext)           \
    {                                                \
        xstring msg = mdlext->GetMessage();          \
        cout << "Invalid File type: " << msg << "\n" \
             << endl;                                \
    }

#define OTK_EXCEPTION_HANDLER(fp_out)                                                                  \
    xcatchbegin                                                                                        \
        xcatch(pfcXToolkitError, pfcex){                                                               \
            OTK_EXCEPTION_PRINT(pfcex)                                                                 \
                OTK_EXCEPTION_WRITE(pfcex, fp_out)} xcatch(pfcXInvalidFileName, pfcex){                \
            OTK_FILENAME_EXCEPTION_PRINT(pfcex)                                                        \
                OTK_FILENAME_EXCEPTION_WRITE(pfcex, fp_out)} xcatch(pfcXUnknownModelExtension, pfcex){ \
            OTK_MDLEXT_EXCEPTION_PRINT(pfcex)                                                          \
                OTK_MDLEXT_EXCEPTION_WRITE(pfcex, fp_out)} xcatchcip(pfcex){                           \
            OTK_DEFAULT_EXCEPTION_PRINT(pfcex)                                                         \
                OTK_DEFAULT_EXCEPTION_WRITE(pfcex, fp_out)} xcatchend

#define OTK_EXCEPTION_TOL(exception_info)                                           \
    xcatchbegin                                                                     \
        xcatch(pfcXToolkitError, pfcex)                                             \
    {                                                                               \
        Tolerance = NULL;                                                           \
        fprintf(exception_info, "msg:%s\n", ((string)pfcex->GetMessage()).c_str()); \
    }                                                                               \
    xcatchend

#endif /* OTKXUTILS_H */
