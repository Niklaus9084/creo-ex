#ifndef otkxlisteners_h_
#define otkxlisteners_h_

#include <OTKXMain.h>
class SessionAction : public virtual pfcSessionActionListener
{
public:
    void OnAfterDirectoryChange(
        xrstring Path) {}

    void OnAfterWindowChange(
        optional pfcWindow_ptr NewWindow);

    void OnAfterModelDisplay();

    void OnBeforeModelErase() {}

    void OnBeforeModelDelete() {}

    void OnBeforeModelRename(
        pfcDescriptorContainer2_ptr Container) {}

    void OnBeforeModelSave(
        pfcDescriptorContainer_ptr Container) {}

    void OnBeforeModelPurge(
        pfcDescriptorContainer_ptr Container) {}

    void OnBeforeModelCopy(
        pfcDescriptorContainer2_ptr Container) {}

    void OnAfterModelPurge(
        pfcModelDescriptor_ptr Descr) {}
};
class ModelAction : public pfcModelActionListener
{
public:
    void OnAfterModelSave(
        pfcModelDescriptor_ptr Descr);

    void OnAfterModelSaveAll(
        pfcModelDescriptor_ptr Descr) {}

    void OnAfterModelCopy(
        pfcModel_ptr FromMdl,
        pfcModel_ptr ToMdl) {}

    void OnAfterModelCopyAll(
        pfcModel_ptr FromMdl,
        pfcModel_ptr ToMdl) {}

    void OnAfterModelRename(
        pfcModel_ptr FromMdl,
        pfcModel_ptr ToMdl);

    void OnAfterModelErase(pfcModel_ptr Mdl) {}

    void OnAfterModelEraseAll(
        pfcModelDescriptor_ptr Descr) {}

    void OnAfterModelDelete(pfcModel_ptr Mdl) {}

    void OnAfterModelDeleteAll(
        pfcModelDescriptor_ptr Descr) {}

    void OnAfterModelRetrieve(pfcModel_ptr Mdl);

    void OnAfterModelRetrieveAll(
        pfcModelDescriptor_ptr Descr) {}

    void OnBeforeModelDisplay(pfcModel_ptr Mdl);

    void OnAfterModelCreate(pfcModel_ptr Mdl) {}

    void OnBeforeParameterCreate(
        pfcModel_ptr Owner,
        xrstring Name,
        pfcParamValue_ptr Value) {}

    void OnBeforeParameterModify(
        pfcParameter_ptr Param,
        pfcParamValue_ptr Value) {}

    void OnBeforeParameterDelete(
        pfcParameter_ptr Param) {}
};
class ModelEvent : public pfcModelEventActionListener
{
public:
    void OnAfterModelCopy(
        pfcModelDescriptor_ptr FromMdl,
        pfcModelDescriptor_ptr ToMdl);

    void OnAfterModelCopyAll(
        pfcModelDescriptor_ptr FromMdl,
        pfcModelDescriptor_ptr ToMdl);

    void OnAfterModelRename(
        pfcModelDescriptor_ptr FromMdl,
        pfcModelDescriptor_ptr ToMdl);

    void OnAfterModelErase(pfcModelDescriptor_ptr Mdl);

    void OnAfterModelDelete(pfcModelDescriptor_ptr Mdl);
};
#endif