#ifndef otkxsketch_h_
#define otkxsketch_h_
#include <OTKXMain.h>
class CrLine
{
private:
    double direct;
    double offset;
    pfcPoint3D_ptr start, end;

public:
    pfcDetailItem_ptr DetailItem;
    void Create(pfcPoint3D_ptr start, pfcPoint3D_ptr end);
    void Change(pfcPoint3D_ptr start, pfcPoint3D_ptr end);
    pfcPoint3D_ptr GetStart();
    pfcPoint3D_ptr GetEnd();
    bool IsHorizon();
    bool IsVertical();
    double GetDirect();
    double GetOffset();
    void ExtendToPoint(pfcPoint3D_ptr aim);
};
void CrLine::Create(pfcPoint3D_ptr start, pfcPoint3D_ptr end)
{
    this->start = start;
    this->end = end;
    this->direct = this->GetDirect();
    this->offset = this->GetOffset();
}
void CrLine::Change(pfcPoint3D_ptr start, pfcPoint3D_ptr end)
{
    this->start = start;
    this->end = end;
}
pfcPoint3D_ptr CrLine::GetStart()
{
    return this->start;
}
pfcPoint3D_ptr CrLine::GetEnd()
{
    return this->end;
}
double CrLine::GetDirect()
{
    double dx, dy;
    dx = start->get(0) - end->get(0);
    dy = start->get(1) - end->get(1);
    return dy / dx;
}
double CrLine::GetOffset()
{
    return (start->get(1) - this->direct * start->get(0));
}
bool CrLine::IsHorizon()
{
    if (abs(direct - 0.0) < eps)
        return true;
    else
        return false;
}
bool CrLine::IsVertical()
{
    if (!IsFiniteNumber(direct))
        return true;
    else
        return false;
}
void CrLine::ExtendToPoint(pfcPoint3D_ptr aim)
{
    OPENEXCEPTIONFILE("a");

    fprintf(exception_info, "aim:");
    for (xint j = 0; j < 3; j++)
    {
        fprintf(exception_info, "%f\t", aim->get(j));
    }
    fprintf(exception_info, "\n");

    if (IsHorizon())
    {
        aim->set(1, this->start->get(1));
    }
    else if (IsVertical())
    {
        aim->set(0, this->start->get(0));
    }
    else
    {
        double aoffset = aim->get(1) + aim->get(0) / direct;
        double ax = direct * (aoffset - offset) / (direct * direct + 1);
        double ay = direct * ax + offset;
        aim->set(0, ax);
        aim->set(1, ay);
    }
    double d1, d2;
    d1 = Cal_Distance(start, aim);
    d2 = Cal_Distance(end, aim);
    if (d1 < d2)
    {
        this->start = aim;
    }
    else
    {
        this->end = aim;
    }
    fclose(exception_info);
}
#endif