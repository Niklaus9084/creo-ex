﻿# Project Name
PROJECTNAME = CreoEx

# File Name
MAKEFILENAME = makefile

# Machine Type
PRO_MACHINE_TYPE = x86e_win64

# Executable names
EXE = $(PROJECTNAME).exe
EXE_DLL = $(PROJECTNAME).dll

# Pro/Toolkit Source & Machine Loadpoint
PROTOOL_SRC = C:/PROGRA~1/PTC/CREO3~1.0/M130/COMMON~1/PROTOO~1
PROTOOL_SYS = $(PROTOOL_SRC)/$(PRO_MACHINE_TYPE)

OTK_CPP_SRC = E:/Creo-ex/src
OTK_CPP_ROOT = C:/PROGRA~1/PTC/CREO3~1.0/M130/COMMON~1/OTK/OTK_CPP
OTK_CPP_SYS = $(OTK_CPP_ROOT)/$(PRO_MACHINE_TYPE)
OTK_CPP_INCLUDES = $(OTK_CPP_ROOT)/include



ICU_PATH = $(PROTOOL_SYS)/obj
#C:\Program Files (x86)\Windows Kits\8.0\Include
SYS_INCLUDE = C:/Progra~2/WI3CF2~1/8.0/Include
#C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC
VS_INCLUDE = C:/Progra~2/MICROS~3.0/VC

# Include File Paths
INCS = -I. \
		-I$(PROTOOL_SRC)/includes 		\
		-I$(OTK_CPP_INCLUDES) 			\
		-I$(OTK_CPP_SRC)/includes		\
		-I$(SYS_INCLUDE)/um 			\
		-I$(SYS_INCLUDE)/shared 		\
		-I$(VS_INCLUDE)/include 		\
		-I$(VS_INCLUDE)/atlmfc/include 	\
		-I$(OTK_CPP_SRC)/includes/xlnt 	\
		-I$(OTK_CPP_SRC)/includes/BasicExcel

# Compiler Flags
CC = "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\bin\amd64\cl.exe"
# CC = cl
# DEBUG = /Zi
MACH = -DPRO_MACHINE=36 -DPRO_OS=4
# CCFLAGS = -c -GS -fp:precise -DUSE_ANSI_IOSTREAMS -DPRO_USE_VAR_ARGS /EHsc $(DEBUG)
CCFLAGS = -c -GS -fp:strict /EHsc /O2
CFLAGS = $(CCFLAGS) $(INCS) $(MACH)

# Libraries
#C:\Program Files (x86)\Windows Kits\8.0
LIB_PATH = C:/Progra~2/WI3CF2~1/8.0/Lib/win8
LIBS = $(LIB_PATH)/um/x64/kernel32.lib  \
		$(LIB_PATH)/um/x64/user32.lib   \
		$(LIB_PATH)/um/x64/wsock32.lib  \
		$(LIB_PATH)/um/x64/advapi32.lib \
		$(LIB_PATH)/um/x64/mpr.lib      \
		$(LIB_PATH)/um/x64/winspool.lib \
		$(LIB_PATH)/um/x64/netapi32.lib \
		$(LIB_PATH)/um/x64/psapi.lib    \
		$(LIB_PATH)/um/x64/gdi32.lib    \
		$(LIB_PATH)/um/x64/shell32.lib  \
		$(LIB_PATH)/um/x64/comdlg32.lib \
		$(LIB_PATH)/um/x64/ole32.lib    \
		$(LIB_PATH)/um/x64/ws2_32.lib	 \
		$(LIB_PATH)/um/x64/OleAut32.Lib	 \
		$(LIB_PATH)/um/x64/shlwapi.lib	 \
		$(VS_INCLUDE)/lib/amd64/libcpmt.lib\
		$(VS_INCLUDE)/lib/amd64/libcmt.lib\
		$(VS_INCLUDE)/lib/amd64/oldnames.lib\
		$(LIB_PATH)/um/x64/uuid.lib	 \
		$(LIB_PATH)/um/x64/version.lib	\
		$(VS_INCLUDE)/atlmfc/lib/amd64/atlsn.lib 

PTCLIBS = \
			$(OTK_CPP_SYS)/obj/otk_cpp.lib        \
			$(ICU_PATH)/protoolkit.lib         \
			$(ICU_PATH)/ucore.lib                 \
			$(ICU_PATH)/udata.lib 				\
			$(OTK_CPP_SRC)/includes/BasicExcel/ExcelFormat.lib
		  		  
PTCLIBS_DLL = \
		   $(OTK_CPP_SYS)/obj/otk_cpp.lib      \
		   $(ICU_PATH)/protk_dll_NU.lib        \
		   $(ICU_PATH)/ucore.lib               \
		   $(ICU_PATH)/udata.lib 				\
		   $(OTK_CPP_SRC)/includes/BasicExcel/ExcelFormat.lib


# Object files
OBJS = CreoEx_main.obj 
# $(OTK_CPP_SRC)/includes/BasicExcel.obj $(OTK_CPP_SRC)/includes/ExcelFormat.obj

# Linker
LINK = "C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\bin\amd64\link.exe"
# LINK = link
#Executable applications compiled using WinMain() instead of main() should set the subsystem to windows instead of console.
LINKFLAGS = /subsystem:console /debug:none /machine:amd64 /ignore:4049 /ignore:4219 /ignore:4217

$(EXE) :  $(OBJS) $(PTCLIBS)
	@echo Start at `date`
	@echo PROTOOL_SRC = $(PROTOOL_SRC)
	@echo PROTOOL_SYS = $(PROTOOL_SYS)
#Executable applications compiled using WinMain() instead of main() should set the subsystem to windows instead of console.
	$(LINK) /out:$(EXE) $(LINKFLAGS) @<<longline.list
$(OBJS) $(PTCLIBS) $(LIBS)
<<	

	@echo Finished at `date`
	del $(OBJS)
	

$(EXE_DLL) :  $(OBJS) $(PTCLIBS_DLL)
	@echo Start at `date`
	@echo PROTOOL_SRC = $(PROTOOL_SRC)
	@echo PROTOOL_SYS = $(PROTOOL_SYS)
	$(LINK) /out:$(EXE_DLL) $(LINKFLAGS) /dll @<<longline.list
$(OBJS) $(PTCLIBS_DLL) $(LIBS)
<<

	@echo Finished at `date`
	del $(OBJS)
	del $(PROJECTNAME).lib
	del $(PROJECTNAME).exp
	move /Y $(EXE_DLL) ./CreoEx

	
# object dependencies

CreoEx_main.obj:  $(OTK_CPP_SRC)/CreoEx_main.cxx
	$(CC) $(CFLAGS) $(OTK_CPP_SRC)/CreoEx_main.cxx

dll: $(EXE_DLL)

clean :
	del $(OBJS)
	del $(EXE)
	nmake -f $(MAKEFILENAME)

clean_dll :
	del $(OBJS)
	del $(EXE_DLL)
	nmake -f $(MAKEFILENAME) dll
